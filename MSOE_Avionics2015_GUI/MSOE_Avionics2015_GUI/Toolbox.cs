﻿//-------------------------------------------------------------------
// @author Kaja, Kyle <kajak@msoe.edu>
//
// @file Toolbox.cs
//
// @description Form code for the toolbox form. The buttons in this
//    form start the avionic tools in separate windows. The position
//    of the open windows can be saved into a template for future
//    use.
//-------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using System.Windows;

namespace MSOE_Avionics2015_GUI
{
  public partial class Toolbox : Form
  {
    //---------------------------------------------------------------
    // If true, that means the window for the corresponding button
    //  is showing. Prevents multiple instances of the same tool's
    //  window to start.
    //---------------------------------------------------------------
    public bool ShowConnect = false;
    public bool ShowAltimeter = false;
    public bool ShowGPS = false;
    public bool ShowCompass = false;
    public bool ShowSpeed = false;
    public bool ShowFPV = false;
    public bool ShowPayload = false;

    //---------------------------------------------------------------
    // These are the indexes of the tools within the Tools List.
    //  They will be set when the toolbox initializes.
    //---------------------------------------------------------------
    public int AltimeterIndex = -1;
    public int GPSIndex = -1;
    public int CompassIndex = -1;
    public int SpeedIndex = -1;
    public int FPVIndex = -1;
    public int PayloadIndex = -1;

    //---------------------------------------------------------------
    // List containing open form instances. Used to keep a connection
    //  to tool instances.
    //---------------------------------------------------------------
    // TODO: Add way to keep instances in a list

    //---------------------------------------------------------------
    // Various other 
    //---------------------------------------------------------------
    public Comms targetComms;
    public List<Avionic_Tool> Tools = new List<Avionic_Tool>();
    public FileIO file;

    public Toolbox()
    {
      InitializeComponent();

      initTools();

      targetComms = new Comms(this);

      file = new FileIO();
      file.newFile();
    }

    private void initTools()
    {
      int i = 0;

      // Add supported tools to the tool list
      Tools.Add(new AvionicTools.Altimeter());
      AltimeterIndex = i++;
      Tools.Add(new AvionicTools.GPS());
      GPSIndex = i++;
      Tools.Add(new AvionicTools.Compass());
      CompassIndex = i++;
      Tools.Add(new AvionicTools.Speed());
      SpeedIndex = i++;
      Tools.Add(new AvionicTools.FPV());
      FPVIndex = i++;
      Tools.Add(new AvionicTools.Payload());
      PayloadIndex = i++;

      // Initialize each tool in the list
      foreach (Avionic_Tool a_t in Tools)
      {
        a_t.Tool_Toolbox = this;
        a_t.Tool_initTool(this);
      }
    }

    public void updateTools(recStruct dataIn) //Method updates each tool when data is received
    {
      for (int i = 0; i < Tools.Count; i++)
      {
        Tools[i].Tool_update(dataIn); //Update each tool
      }
    }

    private void Altimeter_Click(object sender, EventArgs e)
    {
      if (ShowAltimeter)
      {
        // Altimeter form is already shown.
        return;
      }

      if (!Tools[AltimeterIndex].Tool_Form.Created)
      {
        Tools[AltimeterIndex].Tool_initTool(this);
      }
      Tools[AltimeterIndex].Tool_open();
      ShowAltimeter = true;
    }

    private void GPS_Click(object sender, EventArgs e)
    {
      if (ShowGPS)
      {
        // GPS form is already shown.
        return;
      }

      if (!Tools[GPSIndex].Tool_Form.Created)
      {
        Tools[GPSIndex].Tool_initTool(this);
      }
      Tools[GPSIndex].Tool_open();
      Tools[GPSIndex].Tool_Form.Left = this.Left + this.Width;
      Tools[GPSIndex].Tool_Form.Top = this.Top;
      ShowGPS = true;
    }

    private void Compass_Click(object sender, EventArgs e)
    {
      if (ShowCompass)
      {
        // Compass form is already shown.
        return;
      }

      if (!Tools[CompassIndex].Tool_Form.Created)
      {
        Tools[CompassIndex].Tool_initTool(this);
      }
      Tools[CompassIndex].Tool_open();
      ShowCompass = true;
    }

    private void Speed_Click(object sender, EventArgs e)
    {
      if (ShowSpeed)
      {
        // Speed form is already shown.
        return;
      }

      if (!Tools[SpeedIndex].Tool_Form.Created)
      {
        Tools[SpeedIndex].Tool_initTool(this);
      }
      Tools[SpeedIndex].Tool_open();
      ShowSpeed = true;
    }

    private void FPV_Click(object sender, EventArgs e)
    {
      if (ShowFPV)
      {
        // FPV form is already shown.
        return;
      }

      if (!Tools[FPVIndex].Tool_Form.Created)
      {
        Tools[FPVIndex].Tool_initTool(this);
      }
      Tools[FPVIndex].Tool_open();
      ShowFPV = true;
    }

    private void PayloadControl_Click(object sender, EventArgs e)
    {
      if (ShowPayload)
      {
        // Payload Control form is already shown.
        return;
      }

      if (!Tools[PayloadIndex].Tool_Form.Created)
      {
        Tools[PayloadIndex].Tool_initTool(this);
      }
      Tools[PayloadIndex].Tool_open();
      ShowPayload = true;
    }

    private void btnGetSerialPorts_Click(object sender, EventArgs e)
    {
      cboPortList.Items.Clear();
      string[] ArrayComPortsNames = null;
      int index = -1;
      string ComPortName = null;

      //Com Ports
      ArrayComPortsNames = SerialPort.GetPortNames();
      if (ArrayComPortsNames.Count() > 0)
      {
        do
        {
          index += 1;
          cboPortList.Items.Add(ArrayComPortsNames[index]);
        } while (!((ArrayComPortsNames[index] == ComPortName) ||
          (index == ArrayComPortsNames.GetUpperBound(0))));
        Array.Sort(ArrayComPortsNames);

        if (index == ArrayComPortsNames.GetUpperBound(0))
        {
          ComPortName = ArrayComPortsNames[0];
        }

        //get first item print in text
        cboPortList.Text = ArrayComPortsNames[0];
      }
    }

    private void btnOpenPorts_Click(object sender, EventArgs e)
    {
      if (!cboPortList.Text.Equals(""))
      {
        if (btnOpenPorts.Text == "Closed")
        {
          try
          {
            targetComms.xbeeCom.PortName = Convert.ToString(cboPortList.Text);
            targetComms.xbeeCom.Open();
          }
          catch
          {
            return;
          }

          btnOpenPorts.Text = "Open";
#if false
          // Begin Dev Test -- 2/1/2016: kkaja
          targetComms.xbeeCom.Write("Hello from Milwaukee!\r\n");
          transStruct txData;
          txData.armed = 1;
          txData.calAltimeter = 1;
          txData.changeDropLocation = 0;
          txData.headwind = 1;
          txData.manDrop = 0;
          txData.newLat.degrees = 60;
          txData.newLat.minutes = 25.600932f;
          txData.newLat.indicator = 0;
          txData.newLong.degrees = 50;
          txData.newLong.minutes = 35.608832f;
          txData.newLong.indicator = 1;
          txData.packetsToDrop = 2;
          txData.windspeed = 32.42f;
          targetComms.transmit(txData);
#endif // Disable temporary dev test
        }
        else if (btnOpenPorts.Text == "Open")
        {
          btnOpenPorts.Text = "Closed";
          targetComms.startComms();
        }
      }
    }

    private void Toolbox_FormClosing(object sender, FormClosingEventArgs e)
    {
      foreach (Avionic_Tool at in Tools)
      {
        at.Tool_close();
      }
    }

    private void Test_Click(object sender, EventArgs e)
    {
      //transStruct sendTestData = new transStruct();
      //sendTestData.armed = 1;
      //sendTestData.calAltimeter = 1;
      //sendTestData.changeDropLocation = 0;
      //sendTestData.headwind = 1;
      //sendTestData.manDrop = 0;
      //sendTestData.newLat.degrees = 60;
      //sendTestData.newLat.minutes = 25.600932f;
      //sendTestData.newLat.indicator = 0;
      //sendTestData.newLong.degrees = 50;
      //sendTestData.newLong.minutes = 35.608832f;
      //sendTestData.newLong.indicator = 1;
      //sendTestData.packetsToDrop = 2;
      //sendTestData.windspeed = 32.42f;
      //sendTestData.armed = 1;
      //sendTestData.calAltimeter = 1;
      //sendTestData.changeDropLocation = 1;
      //sendTestData.headwind = 1;
      //sendTestData.manDrop = 1;
      //sendTestData.newLat.degrees = 1;
      //sendTestData.newLat.minutes = 1;
      //sendTestData.newLat.indicator = 1;
      //sendTestData.newLong.degrees = 1;
      //sendTestData.newLong.minutes = 1;
      //sendTestData.newLong.indicator = 1;
      //sendTestData.packetsToDrop = 1;
      //sendTestData.windspeed = 1;
      //targetComms.transmit(sendTestData);
      targetComms.test_IO();
    }

    private void btn_replay_Click(object sender, EventArgs e)
    {
      foreach (Avionic_Tool tool in Tools)
      {
        if (tool.isOpen)
        {
         tool.reset();          
        }
      }
      List<recStruct> replayData = null;
      replayData = file.fileReplay();
      foreach (recStruct data in replayData)
      {
        updateTools(data);
        System.Threading.Thread.Sleep(100);
      }
    }
    //tests the above method
    public void test_btn_replay()
    {
      //in case we need to update map here
      List<recStruct> replayData = null;
      replayData = file.fileReplay();
      foreach (recStruct data in replayData)
      {
        updateTools(data);
        System.Threading.Thread.Sleep(250);//wait a while
      }
    }

    private void newRunToolStripMenuItem_Click(object sender, EventArgs e)
    {
      file.newFile();
    }

    private void saveTemplateToolStripMenuItem_Click(object sender, EventArgs e)
    {
      SaveFileDialog templateSave = new SaveFileDialog();
      if (templateSave.ShowDialog() == DialogResult.OK)
      {
        StreamWriter templateStream = new StreamWriter(templateSave.FileName);
        for(int i = 0; i < Application.OpenForms.Count; i++)
        {
          templateStream.Write(Application.OpenForms[i].Name);
          templateStream.Write(",");
          templateStream.Write(Application.OpenForms[i].Location.X);
          templateStream.Write(",");
          templateStream.Write(Application.OpenForms[i].Location.Y);
          templateStream.Write(",");
          templateStream.Write(Application.OpenForms[i].Size.Height);
          templateStream.Write(",");
          templateStream.WriteLine(Application.OpenForms[i].Size.Width);
        }
        templateStream.Close();
      }
    }

    private void openTemplateToolStripMenuItem_Click(object sender, EventArgs e)
    {
      OpenFileDialog templateOpen = new OpenFileDialog();
      if(templateOpen.ShowDialog() == DialogResult.OK)
      {
        while (Application.OpenForms.Count > 1)
        {
          Application.OpenForms[1].Close();
        }

        StreamReader templateStream = new StreamReader(templateOpen.FileName);
        string templateString;
        while((templateString = templateStream.ReadLine()) != null)
        {
          string[] parsed = templateString.Split(',');
          string windowName = parsed[0];
          Point windowLocation = new Point(int.Parse(parsed[1]),int.Parse(parsed[2]));
          Size windowSize = new Size(int.Parse(parsed[4]), int.Parse(parsed[3]));

          if(parsed[0].Equals("compassform",StringComparison.OrdinalIgnoreCase))
          {
            if (!Tools[CompassIndex].Tool_Form.Created)
            {
              Tools[CompassIndex].Tool_initTool(this);
            }
            Tools[CompassIndex].Tool_Form.Location = windowLocation;
            Tools[CompassIndex].Tool_Form.Size = windowSize;
            Tools[CompassIndex].Tool_open();
            ShowCompass = true;
          }
          else if(parsed[0].Equals("gpsform",StringComparison.OrdinalIgnoreCase))
          {
            if (!Tools[GPSIndex].Tool_Form.Created)
            {
              Tools[GPSIndex].Tool_initTool(this);
            }
            Tools[GPSIndex].Tool_Form.Location = windowLocation;
            Tools[GPSIndex].Tool_Form.Size = windowSize;
            Tools[GPSIndex].Tool_open();
            ShowGPS = true;
          }
          else if(parsed[0].Equals("altimeterform", StringComparison.OrdinalIgnoreCase))
          {
            if (!Tools[AltimeterIndex].Tool_Form.Created)
            {
              Tools[AltimeterIndex].Tool_initTool(this);
            }
            Tools[AltimeterIndex].Tool_Form.Location = windowLocation;
            Tools[AltimeterIndex].Tool_Form.Size = windowSize;
            Tools[AltimeterIndex].Tool_open();
            ShowAltimeter = true;
          }
          else if(parsed[0].Equals("payloadform",StringComparison.OrdinalIgnoreCase))
          {
            if (!Tools[PayloadIndex].Tool_Form.Created)
            {
              Tools[PayloadIndex].Tool_initTool(this);
            }
            Tools[PayloadIndex].Tool_Form.Location = windowLocation;
            Tools[PayloadIndex].Tool_Form.Size = windowSize;
            Tools[PayloadIndex].Tool_open();
            ShowPayload = true;
          }
          else if(parsed[0].Equals("fpvform",StringComparison.OrdinalIgnoreCase))
          {
            if (!Tools[FPVIndex].Tool_Form.Created)
            {
              Tools[FPVIndex].Tool_initTool(this);
            }
            Tools[FPVIndex].Tool_Form.Location = windowLocation;
            Tools[FPVIndex].Tool_Form.Size = windowSize;
            Tools[FPVIndex].Tool_open();
            ShowFPV = true;
          }
          else if(parsed[0].Equals("speedform",StringComparison.OrdinalIgnoreCase))
          {
            if (!Tools[SpeedIndex].Tool_Form.Created)
            {
              Tools[SpeedIndex].Tool_initTool(this);
            }
            Tools[SpeedIndex].Tool_Form.Location = windowLocation;
            Tools[SpeedIndex].Tool_Form.Size = windowSize;
            Tools[SpeedIndex].Tool_open();
            ShowSpeed = true;
          }
          else if(parsed[0].Equals("toolbox", StringComparison.OrdinalIgnoreCase))
          {
            Location = windowLocation;
            Size = windowSize;
          }

        }
        templateStream.Close();
      }
    }

    private void newToolStripMenuItem_Click(object sender, EventArgs e)
    {
      while(Application.OpenForms.Count > 1)
      {
        Application.OpenForms[1].Close();
      }
    }
  }
}
