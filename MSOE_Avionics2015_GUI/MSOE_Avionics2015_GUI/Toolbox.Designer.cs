﻿namespace MSOE_Avionics2015_GUI
{
  partial class Toolbox
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Toolbox));
      this.menuStrip1 = new System.Windows.Forms.MenuStrip();
      this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.openTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.saveTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.newRunToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.Heading = new System.Windows.Forms.Button();
      this.GPS = new System.Windows.Forms.Button();
      this.Speed = new System.Windows.Forms.Button();
      this.button6 = new System.Windows.Forms.Button();
      this.PayloadControl = new System.Windows.Forms.Button();
      this.Altimeter = new System.Windows.Forms.Button();
      this.btn_replay = new System.Windows.Forms.Button();
      this.Test = new System.Windows.Forms.Button();
      this.cboPortList = new System.Windows.Forms.ComboBox();
      this.btnOpenPorts = new System.Windows.Forms.Button();
      this.btnGetSerialPorts = new System.Windows.Forms.Button();
      this.menuStrip1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.SuspendLayout();
      // 
      // menuStrip1
      // 
      this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
      this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
      this.menuStrip1.Location = new System.Drawing.Point(0, 0);
      this.menuStrip1.Name = "menuStrip1";
      this.menuStrip1.Size = new System.Drawing.Size(187, 28);
      this.menuStrip1.TabIndex = 19;
      this.menuStrip1.Text = "menuStrip1";
      // 
      // fileToolStripMenuItem
      // 
      this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openTemplateToolStripMenuItem,
            this.saveTemplateToolStripMenuItem,
            this.toolStripSeparator2,
            this.newRunToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
      this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
      this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
      this.fileToolStripMenuItem.Text = "File";
      // 
      // newToolStripMenuItem
      // 
      this.newToolStripMenuItem.Name = "newToolStripMenuItem";
      this.newToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
      this.newToolStripMenuItem.Text = "New Template";
      this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
      // 
      // openTemplateToolStripMenuItem
      // 
      this.openTemplateToolStripMenuItem.Name = "openTemplateToolStripMenuItem";
      this.openTemplateToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
      this.openTemplateToolStripMenuItem.Text = "Open Template";
      this.openTemplateToolStripMenuItem.Click += new System.EventHandler(this.openTemplateToolStripMenuItem_Click);
      // 
      // saveTemplateToolStripMenuItem
      // 
      this.saveTemplateToolStripMenuItem.Name = "saveTemplateToolStripMenuItem";
      this.saveTemplateToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
      this.saveTemplateToolStripMenuItem.Text = "Save Template";
      this.saveTemplateToolStripMenuItem.Click += new System.EventHandler(this.saveTemplateToolStripMenuItem_Click);
      // 
      // toolStripSeparator2
      // 
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new System.Drawing.Size(184, 6);
      // 
      // newRunToolStripMenuItem
      // 
      this.newRunToolStripMenuItem.Name = "newRunToolStripMenuItem";
      this.newRunToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
      this.newRunToolStripMenuItem.Text = "New Run";
      this.newRunToolStripMenuItem.Click += new System.EventHandler(this.newRunToolStripMenuItem_Click);
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new System.Drawing.Size(184, 6);
      // 
      // exitToolStripMenuItem
      // 
      this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
      this.exitToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
      this.exitToolStripMenuItem.Text = "Exit";
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(0, 28);
      this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.Heading);
      this.splitContainer1.Panel1.Controls.Add(this.GPS);
      this.splitContainer1.Panel1.Controls.Add(this.Speed);
      this.splitContainer1.Panel1.Controls.Add(this.button6);
      this.splitContainer1.Panel1.Controls.Add(this.PayloadControl);
      this.splitContainer1.Panel1.Controls.Add(this.Altimeter);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.btn_replay);
      this.splitContainer1.Panel2.Controls.Add(this.Test);
      this.splitContainer1.Panel2.Controls.Add(this.cboPortList);
      this.splitContainer1.Panel2.Controls.Add(this.btnOpenPorts);
      this.splitContainer1.Panel2.Controls.Add(this.btnGetSerialPorts);
      this.splitContainer1.Panel2MinSize = 100;
      this.splitContainer1.Size = new System.Drawing.Size(187, 362);
      this.splitContainer1.SplitterDistance = 258;
      this.splitContainer1.TabIndex = 20;
      // 
      // Heading
      // 
      this.Heading.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.Heading.Location = new System.Drawing.Point(15, 93);
      this.Heading.Name = "Heading";
      this.Heading.Size = new System.Drawing.Size(154, 32);
      this.Heading.TabIndex = 3;
      this.Heading.Text = "Compass";
      this.Heading.UseVisualStyleBackColor = true;
      this.Heading.Click += new System.EventHandler(this.Compass_Click);
      // 
      // GPS
      // 
      this.GPS.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.GPS.Location = new System.Drawing.Point(15, 55);
      this.GPS.Name = "GPS";
      this.GPS.Size = new System.Drawing.Size(154, 32);
      this.GPS.TabIndex = 2;
      this.GPS.Text = "GPS";
      this.GPS.UseVisualStyleBackColor = true;
      this.GPS.Click += new System.EventHandler(this.GPS_Click);
      // 
      // Speed
      // 
      this.Speed.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.Speed.Location = new System.Drawing.Point(15, 131);
      this.Speed.Name = "Speed";
      this.Speed.Size = new System.Drawing.Size(154, 32);
      this.Speed.TabIndex = 4;
      this.Speed.Text = "Speed";
      this.Speed.UseVisualStyleBackColor = true;
      this.Speed.Click += new System.EventHandler(this.Speed_Click);
      // 
      // button6
      // 
      this.button6.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.button6.Location = new System.Drawing.Point(15, 169);
      this.button6.Name = "button6";
      this.button6.Size = new System.Drawing.Size(154, 32);
      this.button6.TabIndex = 5;
      this.button6.Text = "FPV";
      this.button6.UseVisualStyleBackColor = true;
      this.button6.Click += new System.EventHandler(this.FPV_Click);
      // 
      // PayloadControl
      // 
      this.PayloadControl.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.PayloadControl.Location = new System.Drawing.Point(15, 207);
      this.PayloadControl.Name = "PayloadControl";
      this.PayloadControl.Size = new System.Drawing.Size(154, 32);
      this.PayloadControl.TabIndex = 6;
      this.PayloadControl.Text = "Payload Control";
      this.PayloadControl.UseVisualStyleBackColor = true;
      this.PayloadControl.Click += new System.EventHandler(this.PayloadControl_Click);
      // 
      // Altimeter
      // 
      this.Altimeter.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.Altimeter.Location = new System.Drawing.Point(15, 17);
      this.Altimeter.Name = "Altimeter";
      this.Altimeter.Size = new System.Drawing.Size(154, 32);
      this.Altimeter.TabIndex = 1;
      this.Altimeter.Text = "Altimeter";
      this.Altimeter.UseVisualStyleBackColor = true;
      this.Altimeter.Click += new System.EventHandler(this.Altimeter_Click);
      // 
      // btn_replay
      // 
      this.btn_replay.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btn_replay.Location = new System.Drawing.Point(94, 36);
      this.btn_replay.Name = "btn_replay";
      this.btn_replay.Size = new System.Drawing.Size(75, 23);
      this.btn_replay.TabIndex = 7;
      this.btn_replay.Text = "Replay";
      this.btn_replay.UseVisualStyleBackColor = true;
      this.btn_replay.Click += new System.EventHandler(this.btn_replay_Click);
      // 
      // Test
      // 
      this.Test.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.Test.Location = new System.Drawing.Point(94, 7);
      this.Test.Name = "Test";
      this.Test.Size = new System.Drawing.Size(75, 23);
      this.Test.TabIndex = 3;
      this.Test.Text = "Test";
      this.Test.UseVisualStyleBackColor = true;
      this.Test.Click += new System.EventHandler(this.Test_Click);
      // 
      // cboPortList
      // 
      this.cboPortList.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.cboPortList.FormattingEnabled = true;
      this.cboPortList.Location = new System.Drawing.Point(15, 65);
      this.cboPortList.Name = "cboPortList";
      this.cboPortList.Size = new System.Drawing.Size(121, 24);
      this.cboPortList.TabIndex = 2;
      // 
      // btnOpenPorts
      // 
      this.btnOpenPorts.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btnOpenPorts.Location = new System.Drawing.Point(15, 36);
      this.btnOpenPorts.Name = "btnOpenPorts";
      this.btnOpenPorts.Size = new System.Drawing.Size(75, 23);
      this.btnOpenPorts.TabIndex = 1;
      this.btnOpenPorts.Text = "Closed";
      this.btnOpenPorts.UseVisualStyleBackColor = true;
      this.btnOpenPorts.Click += new System.EventHandler(this.btnOpenPorts_Click);
      // 
      // btnGetSerialPorts
      // 
      this.btnGetSerialPorts.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btnGetSerialPorts.Location = new System.Drawing.Point(15, 7);
      this.btnGetSerialPorts.Name = "btnGetSerialPorts";
      this.btnGetSerialPorts.Size = new System.Drawing.Size(75, 23);
      this.btnGetSerialPorts.TabIndex = 0;
      this.btnGetSerialPorts.Text = "Ports";
      this.btnGetSerialPorts.UseVisualStyleBackColor = true;
      this.btnGetSerialPorts.Click += new System.EventHandler(this.btnGetSerialPorts_Click);
      // 
      // Toolbox
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(187, 390);
      this.Controls.Add(this.splitContainer1);
      this.Controls.Add(this.menuStrip1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MainMenuStrip = this.menuStrip1;
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(205, 435);
      this.Name = "Toolbox";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "MSOE Avionics 2015";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Toolbox_FormClosing);
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip menuStrip1;
    private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem openTemplateToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem saveTemplateToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.ComboBox cboPortList;
    private System.Windows.Forms.Button btnOpenPorts;
    private System.Windows.Forms.Button btnGetSerialPorts;
    private System.Windows.Forms.Button Heading;
    private System.Windows.Forms.Button GPS;
    private System.Windows.Forms.Button Speed;
    private System.Windows.Forms.Button button6;
    private System.Windows.Forms.Button PayloadControl;
    private System.Windows.Forms.Button Altimeter;
    private System.Windows.Forms.Button Test;
    private System.Windows.Forms.Button btn_replay;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem newRunToolStripMenuItem;
  }
}

