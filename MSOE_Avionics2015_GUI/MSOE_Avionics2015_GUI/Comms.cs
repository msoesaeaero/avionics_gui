﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Forms;

namespace MSOE_Avionics2015_GUI
{
  public class Comms
  {
    public SerialPort xbeeCom;
    public Toolbox _Toolbox;

    public Comms()
    {
      initComms(null);
    }

    public Comms(Toolbox tb)
    {
      initComms(tb);
    }

    public void initComms(Toolbox tb)
    {
      xbeeCom = new SerialPort();
      xbeeCom.DataBits = 8;
      xbeeCom.StopBits = StopBits.One;
      xbeeCom.ReceivedBytesThreshold = 27;
      xbeeCom.ReadTimeout = 1000;
      xbeeCom.WriteTimeout = 1000;
      xbeeCom.BaudRate = 38400;
      xbeeCom.DtrEnable = true;
      xbeeCom.RtsEnable = true;

      if(tb != null)
      {
        xbeeCom.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(receivedData);
        _Toolbox = tb;
      }
    }

    public void startComms()
    {
      xbeeCom.DiscardInBuffer();
      xbeeCom.DiscardOutBuffer();
      if(!xbeeCom.IsOpen)
      {
        xbeeCom.Open();
      }
    }
    public void exitComms()
    {
      if(xbeeCom.IsOpen)
      {
        xbeeCom.Close();
      }
    }

    private void receivedData(object sender, EventArgs e)
    {
      recStruct dataIn = new recStruct();
      if (receive(ref dataIn))
      {
        _Toolbox.file.fileWrite(dataIn);
        _Toolbox.updateTools(dataIn);
      }
    }

    public bool receive(ref recStruct data)
    {
      data = new recStruct();
      int size = 27;
      byte[] Input = new byte[size];

      try
      {
        if (size != xbeeCom.Read(Input, 0, size))
        {
          xbeeCom.DiscardInBuffer();
          return false;
        }
      }
      catch
      {
        return false;
      }

      if(Input[0] != 0x73)
      {
        xbeeCom.DiscardInBuffer();
        return false;
      }

      data.gpsData.latitude.degrees = Input[1];
      data.gpsData.latitude.minutes = BitConverter.ToSingle(Input, 2);
      data.gpsData.latitude.indicator = Input[6];

      data.gpsData.longitude.degrees = Input[7];
      data.gpsData.longitude.minutes = BitConverter.ToSingle(Input, 8);
      data.gpsData.longitude.indicator = Input[12];

      data.gpsData.speed = BitConverter.ToSingle(Input, 13);
      data.gpsData.heading = BitConverter.ToSingle(Input, 17);
      data.gpsData.validity = Input[21];

      data.haveDropped = Input[22];
      data.altitude = BitConverter.ToSingle(Input, 23);

      return true;
    }

    public void transmit(transStruct data)
    {
      int size = 23;
      byte[] temp = new byte[size];

      if (!(xbeeCom.IsOpen))
      {
        return; // Port is not open, we should not try to transmit
      }

      temp[0] = data.armed;
      temp[1] = data.manDrop;
      temp[2] = data.headwind;
      BitConverter.GetBytes(data.windspeed).CopyTo(temp, 3);
      BitConverter.GetBytes(data.packetsToDrop).CopyTo(temp, 7);
      temp[9] = data.changeDropLocation;
      temp[10] = data.newLong.degrees;
      BitConverter.GetBytes(data.newLong.minutes).CopyTo(temp, 11);
      temp[15] = data.newLong.indicator;
      temp[16] = data.newLat.degrees;
      BitConverter.GetBytes(data.newLat.minutes).CopyTo(temp, 17);
      temp[21] = data.newLat.indicator;
      temp[22] = data.calAltimeter;

      try
      {
        xbeeCom.Write(temp, 0, size);
      }
      catch { }
    }

    // Tests File IO and tool Updating
    public void test_IO()
    {
      recStruct[] write = new recStruct[10];
      _Toolbox.file.newFile();

      for (int i = 0; i < 10; i++) // Create the data
      {
        write[i].altitude = 95 + i;
        write[i].haveDropped = i == 9 ? (byte)1 : (byte)0;
        write[i].gpsData.speed = 22 + i;
        write[i].gpsData.heading = 45;
        write[i].gpsData.latitude.degrees = 43;
        write[i].gpsData.latitude.minutes = (float)(2.93 + .001 * i);
        write[i].gpsData.latitude.indicator = (byte)0;
        write[i].gpsData.longitude.degrees = 87;
        write[i].gpsData.longitude.minutes = (float)(53.93 + .001 * i);
        write[i].gpsData.longitude.indicator = (byte)1;
        write[i].gpsData.validity = 1;
      }

      foreach (recStruct info in write) // Write the data and update the tools
      {
        _Toolbox.file.fileWrite(info);
        System.Threading.Thread.Sleep(250); // Wait a while
        _Toolbox.updateTools(info);
      }
      //_Toolbox.test_btn_replay();
    }
  }
 
  public struct gps_latitude
  {
    public byte  degrees;
    public float minutes;
    public byte  indicator; // N = 0, S = 1
  }

  public struct gps_longitude
  {
    public byte  degrees;
    public float minutes;
    public byte  indicator;  // E = 0, W = 1
  }

  public struct gps_data
  {
    public gps_latitude  latitude;
    public gps_longitude longitude;
    public float speed;
    public float heading;
    public byte  validity;
  }

  public struct recStruct
  {
    public byte     header;
    public gps_data gpsData;
    public byte     haveDropped;
    public float    altitude;
  }

  public struct transStruct
  {
    public byte  armed; //bool
    public byte  manDrop; //bool
    public byte  headwind; //bool
    public float windspeed;
    public short packetsToDrop;
    public byte  changeDropLocation; //bool
    public gps_longitude newLong;
    public gps_latitude newLat;
    public byte  calAltimeter;
  }
}
