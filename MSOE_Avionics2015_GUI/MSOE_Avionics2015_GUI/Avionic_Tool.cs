﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSOE_Avionics2015_GUI
{
  public interface Avionic_Tool
  {
    Form Tool_Form
    {
      get;
      set;
    }

    Toolbox Tool_Toolbox
    {
      get;
      set;
    }

    bool isOpen
    {
      get;
      set;
    }

    void Tool_initTool(Toolbox tb);
    void Tool_open();
    void Tool_close();
    void Tool_update(recStruct dataIn);
    void reset();
  }
}
