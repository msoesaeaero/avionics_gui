﻿namespace MSOE_Avionics2015_GUI.AvionicTools
{
  partial class GPSForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gMap = new GMap.NET.WindowsForms.GMapControl();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.tb_zoom = new System.Windows.Forms.TrackBar();
      this.splitContainer2 = new System.Windows.Forms.SplitContainer();
      this.comboGPS = new System.Windows.Forms.ComboBox();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.tb_zoom)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
      this.splitContainer2.Panel1.SuspendLayout();
      this.splitContainer2.Panel2.SuspendLayout();
      this.splitContainer2.SuspendLayout();
      this.SuspendLayout();
      // 
      // gMap
      // 
      this.gMap.Bearing = 0F;
      this.gMap.CanDragMap = true;
      this.gMap.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gMap.EmptyTileColor = System.Drawing.Color.Navy;
      this.gMap.GrayScaleMode = false;
      this.gMap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
      this.gMap.LevelsKeepInMemmory = 5;
      this.gMap.Location = new System.Drawing.Point(0, 0);
      this.gMap.MarkersEnabled = true;
      this.gMap.MaxZoom = 2;
      this.gMap.MinZoom = 2;
      this.gMap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
      this.gMap.Name = "gMap";
      this.gMap.NegativeMode = false;
      this.gMap.PolygonsEnabled = true;
      this.gMap.RetryLoadTile = 0;
      this.gMap.RoutesEnabled = true;
      this.gMap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
      this.gMap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
      this.gMap.ShowTileGridLines = false;
      this.gMap.Size = new System.Drawing.Size(560, 608);
      this.gMap.TabIndex = 0;
      this.gMap.Zoom = 0D;
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.gMap);
      this.splitContainer1.Panel1MinSize = 0;
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
      this.splitContainer1.Panel2MinSize = 20;
      this.splitContainer1.Size = new System.Drawing.Size(690, 608);
      this.splitContainer1.SplitterDistance = 560;
      this.splitContainer1.SplitterWidth = 2;
      this.splitContainer1.TabIndex = 1;
      // 
      // tb_zoom
      // 
      this.tb_zoom.BackColor = System.Drawing.SystemColors.ControlDarkDark;
      this.tb_zoom.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tb_zoom.LargeChange = 1;
      this.tb_zoom.Location = new System.Drawing.Point(0, 0);
      this.tb_zoom.Maximum = 22;
      this.tb_zoom.Minimum = 3;
      this.tb_zoom.Name = "tb_zoom";
      this.tb_zoom.Orientation = System.Windows.Forms.Orientation.Vertical;
      this.tb_zoom.Size = new System.Drawing.Size(128, 566);
      this.tb_zoom.TabIndex = 2;
      this.tb_zoom.Value = 19;
      this.tb_zoom.ValueChanged += new System.EventHandler(this.UpdateZoom);
      // 
      // splitContainer2
      // 
      this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
      this.splitContainer2.IsSplitterFixed = true;
      this.splitContainer2.Location = new System.Drawing.Point(0, 0);
      this.splitContainer2.Name = "splitContainer2";
      this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer2.Panel1
      // 
      this.splitContainer2.Panel1.Controls.Add(this.tb_zoom);
      // 
      // splitContainer2.Panel2
      // 
      this.splitContainer2.Panel2.Controls.Add(this.comboGPS);
      this.splitContainer2.Size = new System.Drawing.Size(128, 608);
      this.splitContainer2.SplitterDistance = 566;
      this.splitContainer2.TabIndex = 3;
      // 
      // comboGPS
      // 
      this.comboGPS.FormattingEnabled = true;
      this.comboGPS.Location = new System.Drawing.Point(4, 3);
      this.comboGPS.Name = "comboGPS";
      this.comboGPS.Size = new System.Drawing.Size(121, 24);
      this.comboGPS.TabIndex = 0;
      this.comboGPS.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
      // 
      // GPSForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.ClientSize = new System.Drawing.Size(690, 608);
      this.Controls.Add(this.splitContainer1);
      this.DoubleBuffered = true;
      this.Location = new System.Drawing.Point(206, 0);
      this.MinimumSize = new System.Drawing.Size(18, 200);
      this.Name = "GPSForm";
      this.ShowIcon = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "GPS";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GPSForm_closeMap);
      this.Shown += new System.EventHandler(this.GPSForm_mapStart);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.tb_zoom)).EndInit();
      this.splitContainer2.Panel1.ResumeLayout(false);
      this.splitContainer2.Panel1.PerformLayout();
      this.splitContainer2.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
      this.splitContainer2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion
    private GMap.NET.WindowsForms.GMapControl gMap;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.TrackBar tb_zoom;
    private System.Windows.Forms.SplitContainer splitContainer2;
    private System.Windows.Forms.ComboBox comboGPS;
  }
}