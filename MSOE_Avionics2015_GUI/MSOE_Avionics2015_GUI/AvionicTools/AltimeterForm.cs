﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSOE_Avionics2015_GUI.AvionicTools
{
  public partial class AltimeterForm : Form
  {
    public AltimeterForm()
    {
      InitializeComponent();
    }

    delegate void setAltitudeCallback(float altitude);

    public void setAltitude(float altitude)
    {
      
      if (this.textBoxAlt.InvokeRequired)
      {
        if(this.IsDisposed)
        {
          return;
        }
        setAltitudeCallback d = new setAltitudeCallback(setAltitude);
        try
        {
          this.Invoke(d, new object[] { altitude });
        }
        catch { }
      }
      else
      {
        this.textBoxAlt.Text = altitude.ToString("f1");
        this.textBoxAlt.Text += " ft";
        this.Refresh();
      }
    }

    delegate void setDroppedCallback(int haveDropped, float altitude);

    public void setDropped(int haveDropped, float altitude)
    {
      if (this.textBoxDrop.InvokeRequired)
      {
        if(this.IsDisposed)
        {
          return;
        }
        setDroppedCallback d = new setDroppedCallback(setDropped);
        try
        {
          this.Invoke(d, new object[] { haveDropped, altitude });
        }
        catch { }
      }
      else
      {
        if (haveDropped > 0)
        {
          this.textBoxDrop.Text = altitude.ToString("f2");
          this.textBoxDrop.Text += " ft";
          this.Refresh();
        }
      }
    }

    public void resetBoxes()
    {
      textBoxDrop.Text = "---";
      textBoxAlt.Text = "---";
    }
  }
}
