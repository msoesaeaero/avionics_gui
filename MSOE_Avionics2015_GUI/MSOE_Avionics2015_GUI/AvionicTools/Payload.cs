﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSOE_Avionics2015_GUI.AvionicTools
{
  partial class PayloadForm
  {
    private Payload _PayloadControl;

    public Payload PayloadControl
    {
      get
      {
        return _PayloadControl;
      }

      set
      {
        _PayloadControl = value;
      }
    }
  }

  public class Payload : Avionic_Tool
  {
    private Form _Tool_Form;
    private Toolbox _Tool_Toolbox;
    private bool _Tool_isOpen;

    private transStruct toTrans;
    private bool isArmed;
    private bool isConnecting;
    private bool isConnected;
    private bool isManualDrop;
    private int manualDropCount;

    public Form Tool_Form
    {
      get
      {
        return _Tool_Form;
      }

      set
      {
        _Tool_Form = value;
      }
    }

    public Toolbox Tool_Toolbox
    {
      get
      {
        return _Tool_Toolbox;
      }

      set
      {
        _Tool_Toolbox = value;
      }
    }
    public bool isOpen
    {
      get
      {
        return _Tool_isOpen;
      }
      set
      {
        _Tool_isOpen = value;
      }
    }
    public Payload()
    {
    }

    public void Tool_initTool(Toolbox tb)
    {
      Tool_Toolbox = tb;
      Tool_Form = new PayloadForm();
      ((PayloadForm)Tool_Form).PayloadControl = this;
      Tool_Form.FormClosed += new FormClosedEventHandler(Tool_Form_FormClosed);

      toTrans = new transStruct();
      isArmed = false;
      isConnecting = false;
      isConnected = false;
      isManualDrop = false;
      _Tool_isOpen = false;
      manualDropCount = 0;
    }

    public void Tool_open()
    {
      if (Tool_Form == null)
      {
        throw new MemberAccessException("Tool Form is null.");
      }
      if (!isOpen)
      {
        isOpen = true;
        Tool_Form.Show();
      }
    }

    public void Tool_close()
    {
      if (Tool_Form == null || Tool_Toolbox == null)
      {
        throw new MemberAccessException("A class member of the Payload tool is null.");
      }

      if (isOpen)
      {
        Tool_Form.Close();
      }
    }

    public void Tool_update(recStruct dataIn)
    {
      if(!Tool_Toolbox.ShowPayload)
      {
        return;
      }

      if(isConnecting) // Expected handshake response -> send second handshake back
      {
        isConnecting = false;
        isConnected = true;
        this.SendToTrans();
        ((PayloadForm)Tool_Form).SetConnectionStatus("Connected");
      }
      else if (isConnected) // Normal packet from either FLIGHTU or FLIGHTA states
      {
        // Send response packet
        if (dataIn.haveDropped != 0)
        {
          this.EnterDisarmed();
          this.SendToTrans();
        }
        else
        {
          this.UpdateToTrans();
          this.SendToTrans();
        }
      } // We should not be receiving data if we have not connected or are not attempting to connect
    }

    private void Tool_Form_FormClosed(object sender, FormClosedEventArgs e)
    {
      if (Tool_Form == null || Tool_Toolbox == null)
      {
        throw new MemberAccessException("A class member of the Payload tool is null.");
      }

      Tool_Toolbox.ShowPayload = false;
      isOpen = false;
    }

    public void Arm()
    {
      if (!isConnected)
      {
        return; // Don't attempt to arm/disarm if we have not connected to the target
      }

      if (isArmed)
      {
        this.EnterDisarmed();
      }
      else
      {
        this.EnterArmed();
      }
    }

    public void Connect()
    {
      if (isConnected || isConnecting)
      {
        return; // Only allow opening of connection when not connected/connecting
      }

      if (Tool_Toolbox.targetComms.xbeeCom.IsOpen)
      {
        Tool_Toolbox.targetComms.xbeeCom.DiscardInBuffer();
      }
      else
      {
        return;
      }

      isConnecting = true;
      ((PayloadForm)Tool_Form).SetConnectionStatus("Connecting...");

      this.UpdateToTrans(); // Empty packet (unarmed state packet)
      this.SendToTrans();
    }

    public void ManualDrop()
    {
      if (isConnected && isArmed)
      {
        isManualDrop = true;
      }
    }

    public void UpdateTarget(string latText, string lngText)
    {
      double lat = 0;
      double lng = 0;

      if(!double.TryParse(latText, out lat) || !double.TryParse(lngText, out lng))
      {
        return;
      }
      else
      {
        if(Tool_Toolbox.ShowGPS)
        {
          ((GPS)(Tool_Toolbox.Tools[Tool_Toolbox.GPSIndex])).SetTarget(lat, lng);
        }
        if(Tool_Toolbox.ShowCompass)
        {
          ((Compass)(Tool_Toolbox.Tools[Tool_Toolbox.CompassIndex])).UpdateTarget(lat, lng);
        }
      }
    }

    private void UpdateToTrans()
    {
      if (!isArmed)
      {
        // Set everything to zero
        toTrans.armed = 0;
        toTrans.changeDropLocation = 0;
        toTrans.headwind = 0;
        toTrans.manDrop = 0;
        toTrans.newLat.degrees = 0;
        toTrans.newLat.minutes = 0;
        toTrans.newLat.indicator = 0;
        toTrans.newLong.degrees = 0;
        toTrans.newLong.minutes = 0;
        toTrans.newLong.indicator = 0;
        toTrans.packetsToDrop = 0;
        toTrans.windspeed = 0;
      }
      else
      {
        // Get info from form and current states
        toTrans = ((PayloadForm)Tool_Form).GetTransStruct();
        toTrans.armed = 1;
        toTrans.manDrop = (byte)(isManualDrop ? 1 : 0);
      }
      
      toTrans.calAltimeter = (byte)(((Altimeter)Tool_Toolbox.Tools[Tool_Toolbox.AltimeterIndex]).CalibrateAltimeter() ? 1 : 0);
    }

    private void EnterDisarmed()
    {
      isArmed = false;
      this.UpdateToTrans();
      ((PayloadForm)Tool_Form).SetPayloadStatus("Disarmed");
      ((PayloadForm)Tool_Form).SetArmButtonText("Arm");
    }

    private void EnterArmed()
    {
      isArmed = true;
      this.UpdateToTrans();
      ((PayloadForm)Tool_Form).SetPayloadStatus("Armed");
      ((PayloadForm)Tool_Form).SetArmButtonText("Disarm");
    }

    private void SendToTrans()
    {
      Tool_Toolbox.targetComms.transmit(toTrans);
      if(isManualDrop && manualDropCount > 10)
      {
        this.EnterDisarmed();
        isManualDrop = false; // Set to false since we don't want to send manual drop multiple times.
        manualDropCount = 0;
      }
      else if(isManualDrop)
      {
        manualDropCount++;
      }
    }

    public void reset()
    {
      PayloadForm form = (PayloadForm)_Tool_Form;
      form.resetForm();
      isManualDrop = false;
      isArmed = false;
      isConnected = false;
      isConnecting = false;
      toTrans = new transStruct();
      UpdateToTrans();
    }
  }
}
