﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;

using DirectShowLib;

#if !USING_NET11
using System.Runtime.InteropServices.ComTypes;
#endif

namespace MSOE_Avionics2015_GUI.AvionicTools
{
  partial class FPVForm
  {
    public FPV _FPVControl;

    public FPV FPVControl
    {
      get
      {
        return _FPVControl;
      }

      set
      {
        _FPVControl = value;
      }
    }

    private void btn_openDevice_Click(object sender, EventArgs e)
    {
      // TODO: Attempt to open the device selected in the combo box.
      System.Collections.ArrayList deviceList = _FPVControl.FindCaptureDeviceList();
      if (cmb_deviceList.Text != "" && cmb_deviceList.Text != null)
      {
        _FPVControl.CaptureVideo(cmb_deviceList.Text, deviceList);
      }
    }

    private void RefreshFPVList_Click(object sender, EventArgs e)
    {
      cmb_deviceList.Items.Clear();
      System.Collections.ArrayList deviceList = _FPVControl.FindCaptureDeviceList();
      for (int i = 0; i < deviceList.Count; i++)
      {
        cmb_deviceList.Items.Add(((DsDevice)deviceList[i]).Name);
      }

      //get first item print in text
      cmb_deviceList.Text = ((DsDevice)deviceList[0]).Name;
    }

    private void FPVForm_Resize(object sender, System.EventArgs e)
    {
      // Stop graph when Form is iconic
      if (this.WindowState == FormWindowState.Minimized)
        _FPVControl.ChangePreviewState(false);

      // Restart Graph when window come back to normal state
      if (this.WindowState == FormWindowState.Normal)
        _FPVControl.ChangePreviewState(true);

      _FPVControl.ResizeVideoWindow();
    }

    protected override void WndProc(ref Message m)
    {
      switch (m.Msg)
      {
        case FPV.WM_GRAPHNOTIFY:
          {
            _FPVControl.HandleGraphEvent();
            break;
          }
      }

      // Pass this message to the video window for notification of system changes
      if (_FPVControl.videoWindow != null)
        _FPVControl.videoWindow.NotifyOwnerMessage(m.HWnd, m.Msg, m.WParam, m.LParam);

      base.WndProc(ref m);
    }
  }

  public class FPV : Avionic_Tool
  {
    private Form _Tool_Form;
    private Toolbox _Tool_Toolbox;
    private bool _Tool_isOpen;

    public Form Tool_Form
    {
      get
      {
        return _Tool_Form;
      }

      set
      {
        _Tool_Form = value;
      }
    }

    public Toolbox Tool_Toolbox
    {
      get
      {
        return _Tool_Toolbox;
      }

      set
      {
        _Tool_Toolbox = value;
      }
    }

    public bool isOpen
    {
      get
      {
        return _Tool_isOpen;
      }
      set
      {
        _Tool_isOpen = value;
      }
    }

    public Int32 FPVVal;

    public FPV()
    {
    }

    public void Tool_initTool(Toolbox tb)
    {
      FPVVal = 0;

      Tool_Form = new FPVForm();
      ((FPVForm)Tool_Form)._FPVControl = this;
      Tool_Form.FormClosed += new FormClosedEventHandler(Tool_Form_FormClosed);
      _Tool_isOpen = false;
    }

    public void Tool_update(recStruct dataIn)
    {
      //Update FPV? 
    }

    public void Tool_open()
    {
      if (Tool_Form == null)
      {
        throw new MemberAccessException("Tool Form is null.");
      }

      if(!isOpen)
      {
        isOpen = true;
        Tool_Form.Show();
      }
    }

    public void Tool_close()
    {
      if (Tool_Form == null || Tool_Toolbox == null)
      {
        throw new MemberAccessException("A class member of the FPV tool is null.");
      }

      if (isOpen)
      {
        CloseInterfaces();
        Tool_Form.Close();
      }
    }

    private void Tool_Form_FormClosed(object sender, FormClosedEventArgs e)
    {
      if (Tool_Form == null || Tool_Toolbox == null)
      {
        throw new MemberAccessException("A class member of the FPV tool is null.");
      }

      Tool_Toolbox.ShowFPV = false;
      isOpen = false;
    }

    // a small enum to record the graph state
    enum PlayState
    {
      Stopped,
      Paused,
      Running,
      Init
    };

    // Application-defined message to notify app of filtergraph events
    public const int WM_GRAPHNOTIFY = 0x8000 + 1;

    public IVideoWindow videoWindow = null;
    IMediaControl mediaControl = null;
    IMediaEventEx mediaEventEx = null;
    IGraphBuilder graphBuilder = null;
    ICaptureGraphBuilder2 captureGraphBuilder = null;
    PlayState currentState = PlayState.Stopped;

    DsROTEntry rot = null;


    public void CaptureVideo(String name, System.Collections.ArrayList list)
    {
      int hr = 0;
      IBaseFilter sourceFilter = null;

      try
      {

        // Get DirectShow interfaces
        GetInterfaces();
        object source;

        // Attach the filter graph to the capture graph
        hr = this.captureGraphBuilder.SetFiltergraph(this.graphBuilder);
        DsError.ThrowExceptionForHR(hr);

        // Use the system device enumerator and class enumerator to find
        // a video capture/preview device, such as a desktop USB video camera.
        DsDevice device = null;
        for (int i = 0; i < list.Count; i++)
        {
          device = name.Equals(((DsDevice)list[i]).Name) ? (DsDevice)list[i] : null;
        }
        // Bind Moniker to a filter object
        Guid iid = typeof(IBaseFilter).GUID;

        device.Mon.BindToObject(null, null, ref iid, out source);

        sourceFilter = (IBaseFilter)source;

        // Add Capture filter to our graph.
        hr = this.graphBuilder.AddFilter(sourceFilter, "Video Capture");
        DsError.ThrowExceptionForHR(hr);

        // Render the preview pin on the video capture filter
        // Use this instead of this.graphBuilder.RenderFile
        hr = this.captureGraphBuilder.RenderStream(PinCategory.Preview, MediaType.Video, sourceFilter, null, null);
        DsError.ThrowExceptionForHR(hr);

        // Now that the filter has been added to the graph and we have
        // rendered its stream, we can release this reference to the filter.
        Marshal.ReleaseComObject(sourceFilter);

        // Set video window style and position
        SetupVideoWindow();

        // Add our graph to the running object table, which will allow
        // the GraphEdit application to "spy" on our graph
        rot = new DsROTEntry(this.graphBuilder);

        // Start previewing video data
        hr = this.mediaControl.Run();
        DsError.ThrowExceptionForHR(hr);

        // Remember current state
        this.currentState = PlayState.Running;
      }
      catch
      {
        MessageBox.Show("An unrecoverable error has occurred.");
      }
    }
    // Uncomment this version of FindCaptureDevice to use the DsDevice helper class
    // (and comment the first version of course)
    public System.Collections.ArrayList FindCaptureDeviceList()
    {
      System.Collections.ArrayList devices = new System.Collections.ArrayList();


      // Get all video input devices
      devices.AddRange(DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice));

      // Take the first device
      return devices;
    }

    public void GetInterfaces()
    {
      int hr = 0;

      // An exception is thrown if cast fail
      this.graphBuilder = (IGraphBuilder)new FilterGraph();
      this.captureGraphBuilder = (ICaptureGraphBuilder2)new CaptureGraphBuilder2();
      this.mediaControl = (IMediaControl)this.graphBuilder;
      this.videoWindow = (IVideoWindow)this.graphBuilder;
      this.mediaEventEx = (IMediaEventEx)this.graphBuilder;

      hr = this.mediaEventEx.SetNotifyWindow(_Tool_Form.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
      DsError.ThrowExceptionForHR(hr);
    }

    public void CloseInterfaces()
    {
      // Stop previewing data
      if (this.mediaControl != null)
        this.mediaControl.StopWhenReady();

      this.currentState = PlayState.Stopped;

      // Stop receiving events
      if (this.mediaEventEx != null)
        this.mediaEventEx.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero);

      // Relinquish ownership (IMPORTANT!) of the video window.
      // Failing to call put_Owner can lead to assert failures within
      // the video renderer, as it still assumes that it has a valid
      // parent window.
      if (this.videoWindow != null)
      {
        this.videoWindow.put_Visible(OABool.False);
        this.videoWindow.put_Owner(IntPtr.Zero);
      }

      // Remove filter graph from the running object table
      if (rot != null)
      {
        rot.Dispose();
        rot = null;
      }

      // Release DirectShow interfaces
      Marshal.ReleaseComObject(this.mediaControl); this.mediaControl = null;
      Marshal.ReleaseComObject(this.mediaEventEx); this.mediaEventEx = null;
      Marshal.ReleaseComObject(this.videoWindow); this.videoWindow = null;
      Marshal.ReleaseComObject(this.graphBuilder); this.graphBuilder = null;
      Marshal.ReleaseComObject(this.captureGraphBuilder); this.captureGraphBuilder = null;
    }

    public void SetupVideoWindow()
    {
      int hr = 0;

      // Set the video window to be a child of the main window
      hr = this.videoWindow.put_Owner(((FPVForm)_Tool_Form)._FPVPanel.Handle);
      DsError.ThrowExceptionForHR(hr);

      hr = this.videoWindow.put_WindowStyle(WindowStyle.Child | WindowStyle.ClipChildren);
      DsError.ThrowExceptionForHR(hr);

      // Use helper function to position video window in client rect 
      // of main application window
      ResizeVideoWindow();

      // Make the video window visible, now that it is properly positioned
      hr = this.videoWindow.put_Visible(OABool.True);
      DsError.ThrowExceptionForHR(hr);
    }

    public void ResizeVideoWindow()
    {
      // Resize the video preview window to match owner window size
      if (this.videoWindow != null)
      {
        this.videoWindow.SetWindowPosition(0, 0, ((FPVForm)_Tool_Form)._FPVPanel.Width, ((FPVForm)_Tool_Form)._FPVPanel.Height);
      }
    }

    public void ChangePreviewState(bool showVideo)
    {
      int hr = 0;

      // If the media control interface isn't ready, don't call it
      if (this.mediaControl == null)
        return;

      if (showVideo)
      {
        if (this.currentState != PlayState.Running)
        {
          // Start previewing video data
          hr = this.mediaControl.Run();
          this.currentState = PlayState.Running;
        }
      }
      else
      {
        // Stop previewing video data
        hr = this.mediaControl.StopWhenReady();
        this.currentState = PlayState.Stopped;
      }
    }

    public void HandleGraphEvent()
    {
      int hr = 0;
      EventCode evCode;
      IntPtr evParam1, evParam2;

      if (this.mediaEventEx == null)
        return;

      while (this.mediaEventEx.GetEvent(out evCode, out evParam1, out evParam2, 0) == 0)
      {
        // Free event parameters to prevent memory leaks associated with
        // event parameter data.  While this application is not interested
        // in the received events, applications should always process them.
        hr = this.mediaEventEx.FreeEventParams(evCode, evParam1, evParam2);
        DsError.ThrowExceptionForHR(hr);

        // Insert event processing code here, if desired
      }
    }
    public void reset()
    {
      FPVForm form = (FPVForm)_Tool_Form;
      //form.resetForm();
    }
  }
}
