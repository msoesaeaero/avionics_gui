﻿namespace MSOE_Avionics2015_GUI.AvionicTools
{
  partial class CompassForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBoxLat = new System.Windows.Forms.TextBox();
      this.textBoxLong = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.textBoxToTarget = new System.Windows.Forms.TextBox();
      this.textBoxHeading = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.SuspendLayout();
      // 
      // textBoxLat
      // 
      this.textBoxLat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.textBoxLat.Enabled = false;
      this.textBoxLat.Location = new System.Drawing.Point(16, 24);
      this.textBoxLat.Name = "textBoxLat";
      this.textBoxLat.ReadOnly = true;
      this.textBoxLat.Size = new System.Drawing.Size(114, 22);
      this.textBoxLat.TabIndex = 0;
      this.textBoxLat.Text = "N/A";
      this.textBoxLat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // textBoxLong
      // 
      this.textBoxLong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.textBoxLong.Enabled = false;
      this.textBoxLong.Location = new System.Drawing.Point(138, 24);
      this.textBoxLong.Name = "textBoxLong";
      this.textBoxLong.ReadOnly = true;
      this.textBoxLong.Size = new System.Drawing.Size(114, 22);
      this.textBoxLong.TabIndex = 1;
      this.textBoxLong.Text = "N/A";
      this.textBoxLong.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(46, 4);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(59, 17);
      this.label1.TabIndex = 2;
      this.label1.Text = "Latitude";
      // 
      // label2
      // 
      this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(159, 4);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(71, 17);
      this.label2.TabIndex = 3;
      this.label2.Text = "Longitude";
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.BackgroundImage = global::MSOE_Avionics2015_GUI.Properties.Resources.Compass_Mock;
      this.splitContainer1.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.splitContainer1.Panel1MinSize = 10;
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.textBoxToTarget);
      this.splitContainer1.Panel2.Controls.Add(this.textBoxHeading);
      this.splitContainer1.Panel2.Controls.Add(this.label4);
      this.splitContainer1.Panel2.Controls.Add(this.label3);
      this.splitContainer1.Panel2.Controls.Add(this.label2);
      this.splitContainer1.Panel2.Controls.Add(this.label1);
      this.splitContainer1.Panel2.Controls.Add(this.textBoxLat);
      this.splitContainer1.Panel2.Controls.Add(this.textBoxLong);
      this.splitContainer1.Panel2MinSize = 60;
      this.splitContainer1.Size = new System.Drawing.Size(269, 327);
      this.splitContainer1.SplitterDistance = 229;
      this.splitContainer1.SplitterWidth = 1;
      this.splitContainer1.TabIndex = 4;
      // 
      // textBoxToTarget
      // 
      this.textBoxToTarget.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.textBoxToTarget.Enabled = false;
      this.textBoxToTarget.Location = new System.Drawing.Point(138, 69);
      this.textBoxToTarget.Name = "textBoxToTarget";
      this.textBoxToTarget.ReadOnly = true;
      this.textBoxToTarget.Size = new System.Drawing.Size(114, 22);
      this.textBoxToTarget.TabIndex = 7;
      this.textBoxToTarget.Text = "N/A";
      this.textBoxToTarget.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // textBoxHeading
      // 
      this.textBoxHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.textBoxHeading.Enabled = false;
      this.textBoxHeading.Location = new System.Drawing.Point(16, 69);
      this.textBoxHeading.Name = "textBoxHeading";
      this.textBoxHeading.ReadOnly = true;
      this.textBoxHeading.Size = new System.Drawing.Size(114, 22);
      this.textBoxHeading.TabIndex = 6;
      this.textBoxHeading.Text = "N/A";
      this.textBoxHeading.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label4
      // 
      this.label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(135, 49);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(123, 17);
      this.label4.TabIndex = 5;
      this.label4.Text = "Heading to Target";
      // 
      // label3
      // 
      this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(46, 49);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(61, 17);
      this.label3.TabIndex = 4;
      this.label3.Text = "Heading";
      // 
      // CompassForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(269, 327);
      this.Controls.Add(this.splitContainer1);
      this.DoubleBuffered = true;
      this.Location = new System.Drawing.Point(1245, 570);
      this.MaximizeBox = false;
      this.MinimumSize = new System.Drawing.Size(260, 346);
      this.Name = "CompassForm";
      this.ShowIcon = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "Compass";
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.Panel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

        #endregion

        private System.Windows.Forms.TextBox textBoxLat;
        private System.Windows.Forms.TextBox textBoxLong;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.TextBox textBoxToTarget;
    private System.Windows.Forms.TextBox textBoxHeading;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
  }
}