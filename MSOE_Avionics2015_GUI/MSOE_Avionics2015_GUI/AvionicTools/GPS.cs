﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSOE_Avionics2015_GUI.AvionicTools
{
  partial class GPSForm
  {
    private GPS _GPSControl;

    public GPS GPSControl
    {
      get
      {
        return _GPSControl;
      }

      set
      {
        _GPSControl = value;
      }
    }
  }

  public class GPS : Avionic_Tool
  {
    private Form _Tool_Form;
    private Toolbox _Tool_Toolbox;
    private bool _Tool_isOpen;


    private bool firstRoute;

    public Form Tool_Form
    {
      get
      {
        return _Tool_Form;
      }

      set
      {
        _Tool_Form = value;
      }
    }

    public Toolbox Tool_Toolbox
    {
      get
      {
        return _Tool_Toolbox;
      }

      set
      {
        _Tool_Toolbox = value;
      }
    }
    public bool isOpen
    {
      get
      {
        return _Tool_isOpen;
      }
      set
      {
        _Tool_isOpen = value;
      }
    }
    public GPS()
    {
    }

    public void Tool_initTool(Toolbox tb)
    {
      firstRoute = true;

      Tool_Toolbox = tb;
      Tool_Form = new GPSForm();
      Tool_Form.FormClosed += new FormClosedEventHandler(Tool_Form_FormClosed);
      _Tool_isOpen = false;
    }

    public void Tool_update(recStruct dataIn)
    {
      GPSForm parent = (GPSForm)Tool_Form;

      if(!Tool_Toolbox.ShowGPS)
      {
        return;
      }

      // Invalid data received
      if(dataIn.gpsData.validity != 1)
      {
        return;
      }

      // Update plane route
      double lat = Math.Pow(-1.0, dataIn.gpsData.latitude.indicator) * (dataIn.gpsData.latitude.degrees + dataIn.gpsData.latitude.minutes / 60);
      double lng = Math.Pow(-1.0, dataIn.gpsData.longitude.indicator) * (dataIn.gpsData.longitude.degrees + dataIn.gpsData.longitude.minutes / 60);

      if (Math.Abs(lat) > 200 || Math.Abs(lng) > 200)
      {
        lat = 1; // Something is amiss (received data has been malformed)
      }

      GMap.NET.PointLatLng pos = new GMap.NET.PointLatLng(lat, lng);
      parent.AddToRoute(pos);

      // If this is the first time the plane route is being set,
      //  change the map's position to be centered on the received point.
      if (firstRoute)
      {
        parent.SetMapPosition(lat, lng);
        firstRoute = false;
      }

      // If the bomb has been dropped, put a marker on the point at which it was dropped.
      if(dataIn.haveDropped == 1)
      {
        parent.AddDropMarker(new GMap.NET.PointLatLng(lat, lng));
      }
      // Update plane sprite position and heading
      parent.updateDirection(dataIn.gpsData.heading);
    }

    public void Tool_open()
    {
      if (Tool_Form == null)
      {
        throw new MemberAccessException("Tool Form is null.");
      }
      if (!isOpen)
      {
        isOpen = true;
        Tool_Form.Show();
      }
    }

    public void Tool_close()
    {
      if (Tool_Form == null || Tool_Toolbox == null)
      {
        throw new MemberAccessException("A class member of the GPS tool is null.");
      }

      if (isOpen)
      {
        Tool_Form.Close();
      }
    }

    private void Tool_Form_FormClosed(object sender, FormClosedEventArgs e)
    {
      if (Tool_Form == null || Tool_Toolbox == null)
      {
        throw new MemberAccessException("A class member of the GPS tool is null.");
      }

      Tool_Toolbox.ShowGPS = false;
      isOpen = false;
    }

    public void SetTarget(double lat, double lng)
    {
      ((GPSForm)Tool_Form).SetTargetLocation(lat, lng);
    }
    public void reset()
    {
      GPSForm tempform = (GPSForm)_Tool_Form;
      tempform.resetOverlay();
    }
  }
}
