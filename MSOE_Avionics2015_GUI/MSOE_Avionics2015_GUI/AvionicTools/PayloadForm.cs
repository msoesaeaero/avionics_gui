﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSOE_Avionics2015_GUI.AvionicTools
{
  public partial class PayloadForm : Form
  {
    public PayloadForm()
    {
      InitializeComponent();
    }

    public transStruct GetTransStruct()
    {
      transStruct temp = new transStruct();

      temp.armed = 0; // Handled by Payload tool
      temp.manDrop = 0; // Handled by Payload tool
      temp.headwind = (byte)(this.rb_headwind.Checked ? 1 : 0);
      temp.windspeed = Single.Parse(this.tb_windspeed.Text);
      temp.packetsToDrop = (short)this.ud_numPayloads.Value;
      temp.changeDropLocation = 0; // Handled by Payload tool - maybe, TBD.
      
      string[] longitude = new string[3];
      longitude = this.tb_lng.Text.Split(new char[] { '-', '.' });
      temp.newLong.degrees = (byte)UInt16.Parse(this.tb_lng.Text.Contains("-") ? longitude[1] : longitude[0]);
      temp.newLong.minutes = Single.Parse(longitude[1]) * 60.0f;
      temp.newLong.indicator = (byte)(this.tb_lng.Text.Contains("-") ? 1 : 0);
      
      string[] latitude = new string[3];
      latitude = this.tb_lat.Text.Split(new char[] { '-', '.' });
      temp.newLat.degrees = (byte)UInt16.Parse(this.tb_lat.Text.Contains("-") ? latitude[1] : latitude[0]);
      temp.newLat.minutes = Single.Parse(latitude[1]) * 60.0f;
      temp.newLat.indicator = (byte)(this.tb_lat.Text.Contains("-") ? 1 : 0);
      temp.calAltimeter = 0; // Handled by Payload tool

      return temp;
    }

    private void Arm_Click(object sender, EventArgs e)
    {
      if(tb_lat.Text == "" || tb_lng.Text == "" || tb_windspeed.Text == "")
      {
        return; // A necessary field was empty
      }

      _PayloadControl.Arm();
    }

    private void btn_updateTarget_Click(object sender, EventArgs e)
    {
      _PayloadControl.UpdateTarget(this.tb_lat.Text, this.tb_lng.Text);
    }

    private void btn_manualDrop_Click(object sender, EventArgs e)
    {
      _PayloadControl.ManualDrop();
    }

    private void btn_connect_Click(object sender, EventArgs e)
    {
      _PayloadControl.Connect();
    }

    delegate void SetConnectionStatusCallback(string connStatus);

    public void SetConnectionStatus(string connStatus)
    {
      if (this.tb_connectionStatus.InvokeRequired)
      {
        if(this.IsDisposed)
        {
          return;
        }
        SetConnectionStatusCallback d = new SetConnectionStatusCallback(SetConnectionStatus);
        try
        {
          this.Invoke(d, new object[] { connStatus });
        }
        catch { }
      }
      else
      {
        this.tb_connectionStatus.Text = connStatus;
        this.Refresh();
      }
    }

    delegate void SetPayloadStatusCallback(string payloadStatus);

    public void SetPayloadStatus(string payloadStatus)
    {
      if (this.tb_connectionStatus.InvokeRequired)
      {
        if(this.IsDisposed)
        {
          return;
        }
        SetPayloadStatusCallback d = new SetPayloadStatusCallback(SetPayloadStatus);
        try
        {
          this.Invoke(d, new object[] { payloadStatus });
        }
        catch { }
      }
      else
      {
        this.tb_payloadStatus.Text = payloadStatus;
        this.Refresh();
      }
    }

    delegate void SetArmButtonTextCallback(string armText);

    public void SetArmButtonText(string armText)
    {
      if (this.tb_connectionStatus.InvokeRequired)
      {
        if(this.IsDisposed)
        {
          return;
        }
        SetArmButtonTextCallback d = new SetArmButtonTextCallback(SetArmButtonText);
        try
        {
          this.Invoke(d, new object[] { armText });
        }
        catch { }
      }
      else
      {
        this.Arm.Text = armText;
        this.Refresh();
      }
    }

    public void resetForm()
    {
      tb_connectionStatus.Text = "Closed";
      tb_payloadStatus.Text = "Disarmed";
      tb_windspeed.Text = "";
      tb_lat.Text = "";
      tb_lng.Text = "";
      rb_headwind.TabStop = true;
      rb_tailwind.TabStop = false;
      ud_numPayloads.Value = 1;
    }
  }
}
