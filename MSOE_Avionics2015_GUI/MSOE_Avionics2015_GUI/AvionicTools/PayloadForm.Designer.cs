﻿namespace MSOE_Avionics2015_GUI.AvionicTools
{
  partial class PayloadForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.Arm = new System.Windows.Forms.Button();
      this.btn_connect = new System.Windows.Forms.Button();
      this.tb_payloadStatus = new System.Windows.Forms.TextBox();
      this.tb_connectionStatus = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.tb_lat = new System.Windows.Forms.TextBox();
      this.ud_numPayloads = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.panel1 = new System.Windows.Forms.Panel();
      this.btn_updateTarget = new System.Windows.Forms.Button();
      this.tb_lng = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.rb_headwind = new System.Windows.Forms.RadioButton();
      this.rb_tailwind = new System.Windows.Forms.RadioButton();
      this.label8 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.tb_windspeed = new System.Windows.Forms.TextBox();
      this.btn_manualDrop = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.ud_numPayloads)).BeginInit();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // Arm
      // 
      this.Arm.Location = new System.Drawing.Point(12, 59);
      this.Arm.Name = "Arm";
      this.Arm.Size = new System.Drawing.Size(94, 74);
      this.Arm.TabIndex = 1;
      this.Arm.Text = "Arm";
      this.Arm.UseVisualStyleBackColor = true;
      this.Arm.Click += new System.EventHandler(this.Arm_Click);
      // 
      // btn_connect
      // 
      this.btn_connect.Location = new System.Drawing.Point(12, 12);
      this.btn_connect.Name = "btn_connect";
      this.btn_connect.Size = new System.Drawing.Size(330, 38);
      this.btn_connect.TabIndex = 0;
      this.btn_connect.Text = "Connect";
      this.btn_connect.UseVisualStyleBackColor = true;
      this.btn_connect.Click += new System.EventHandler(this.btn_connect_Click);
      // 
      // tb_payloadStatus
      // 
      this.tb_payloadStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.tb_payloadStatus.Location = new System.Drawing.Point(203, 111);
      this.tb_payloadStatus.Name = "tb_payloadStatus";
      this.tb_payloadStatus.ReadOnly = true;
      this.tb_payloadStatus.Size = new System.Drawing.Size(135, 22);
      this.tb_payloadStatus.TabIndex = 20;
      this.tb_payloadStatus.Text = "Disarmed";
      this.tb_payloadStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tb_connectionStatus
      // 
      this.tb_connectionStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.tb_connectionStatus.Location = new System.Drawing.Point(203, 83);
      this.tb_connectionStatus.Name = "tb_connectionStatus";
      this.tb_connectionStatus.ReadOnly = true;
      this.tb_connectionStatus.Size = new System.Drawing.Size(135, 22);
      this.tb_connectionStatus.TabIndex = 20;
      this.tb_connectionStatus.Text = "Closed";
      this.tb_connectionStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(200, 59);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(48, 17);
      this.label1.TabIndex = 20;
      this.label1.Text = "Status";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(114, 86);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(83, 17);
      this.label2.TabIndex = 20;
      this.label2.Text = "Connection:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(134, 114);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(63, 17);
      this.label3.TabIndex = 20;
      this.label3.Text = "Payload:";
      // 
      // tb_lat
      // 
      this.tb_lat.Location = new System.Drawing.Point(89, 30);
      this.tb_lat.Name = "tb_lat";
      this.tb_lat.Size = new System.Drawing.Size(225, 22);
      this.tb_lat.TabIndex = 3;
      // 
      // ud_numPayloads
      // 
      this.ud_numPayloads.Location = new System.Drawing.Point(159, 154);
      this.ud_numPayloads.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.ud_numPayloads.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.ud_numPayloads.Name = "ud_numPayloads";
      this.ud_numPayloads.Size = new System.Drawing.Size(38, 22);
      this.ud_numPayloads.TabIndex = 2;
      this.ud_numPayloads.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.ud_numPayloads.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(9, 154);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(140, 17);
      this.label4.TabIndex = 20;
      this.label4.Text = "Number of Payloads:";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(121, 4);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(85, 17);
      this.label5.TabIndex = 20;
      this.label5.Text = "Drop Target";
      // 
      // panel1
      // 
      this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.panel1.Controls.Add(this.btn_updateTarget);
      this.panel1.Controls.Add(this.tb_lng);
      this.panel1.Controls.Add(this.label7);
      this.panel1.Controls.Add(this.label6);
      this.panel1.Controls.Add(this.label5);
      this.panel1.Controls.Add(this.tb_lat);
      this.panel1.Location = new System.Drawing.Point(12, 188);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(326, 131);
      this.panel1.TabIndex = 20;
      // 
      // btn_updateTarget
      // 
      this.btn_updateTarget.Location = new System.Drawing.Point(190, 91);
      this.btn_updateTarget.Name = "btn_updateTarget";
      this.btn_updateTarget.Size = new System.Drawing.Size(124, 30);
      this.btn_updateTarget.TabIndex = 5;
      this.btn_updateTarget.Text = "Update Target";
      this.btn_updateTarget.UseVisualStyleBackColor = true;
      this.btn_updateTarget.Click += new System.EventHandler(this.btn_updateTarget_Click);
      // 
      // tb_lng
      // 
      this.tb_lng.Location = new System.Drawing.Point(89, 62);
      this.tb_lng.Name = "tb_lng";
      this.tb_lng.Size = new System.Drawing.Size(225, 22);
      this.tb_lng.TabIndex = 4;
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(8, 65);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(75, 17);
      this.label7.TabIndex = 20;
      this.label7.Text = "Longitude:";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(20, 33);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(63, 17);
      this.label6.TabIndex = 20;
      this.label6.Text = "Latitude:";
      // 
      // rb_headwind
      // 
      this.rb_headwind.AutoSize = true;
      this.rb_headwind.Checked = true;
      this.rb_headwind.Location = new System.Drawing.Point(58, 352);
      this.rb_headwind.Name = "rb_headwind";
      this.rb_headwind.Size = new System.Drawing.Size(91, 21);
      this.rb_headwind.TabIndex = 6;
      this.rb_headwind.TabStop = true;
      this.rb_headwind.Text = "Headwind";
      this.rb_headwind.UseVisualStyleBackColor = true;
      // 
      // rb_tailwind
      // 
      this.rb_tailwind.AutoSize = true;
      this.rb_tailwind.Location = new System.Drawing.Point(203, 352);
      this.rb_tailwind.Name = "rb_tailwind";
      this.rb_tailwind.Size = new System.Drawing.Size(80, 21);
      this.rb_tailwind.TabIndex = 7;
      this.rb_tailwind.Text = "Tailwind";
      this.rb_tailwind.UseVisualStyleBackColor = true;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(114, 327);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(129, 17);
      this.label8.TabIndex = 20;
      this.label8.Text = "Approach Direction";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(15, 383);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(124, 17);
      this.label9.TabIndex = 20;
      this.label9.Text = "Windspeed (mph):";
      // 
      // tb_windspeed
      // 
      this.tb_windspeed.Location = new System.Drawing.Point(145, 380);
      this.tb_windspeed.MaxLength = 12;
      this.tb_windspeed.Name = "tb_windspeed";
      this.tb_windspeed.Size = new System.Drawing.Size(193, 22);
      this.tb_windspeed.TabIndex = 8;
      // 
      // btn_manualDrop
      // 
      this.btn_manualDrop.Location = new System.Drawing.Point(108, 411);
      this.btn_manualDrop.Name = "btn_manualDrop";
      this.btn_manualDrop.Size = new System.Drawing.Size(137, 47);
      this.btn_manualDrop.TabIndex = 9;
      this.btn_manualDrop.Text = "Manual Drop Caution!";
      this.btn_manualDrop.UseVisualStyleBackColor = true;
      this.btn_manualDrop.Click += new System.EventHandler(this.btn_manualDrop_Click);
      // 
      // PayloadForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(355, 467);
      this.Controls.Add(this.btn_manualDrop);
      this.Controls.Add(this.tb_windspeed);
      this.Controls.Add(this.label9);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.rb_tailwind);
      this.Controls.Add(this.rb_headwind);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.ud_numPayloads);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.tb_connectionStatus);
      this.Controls.Add(this.tb_payloadStatus);
      this.Controls.Add(this.Arm);
      this.Controls.Add(this.btn_connect);
      this.Location = new System.Drawing.Point(1545, 0);
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(373, 512);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(373, 512);
      this.Name = "PayloadForm";
      this.ShowIcon = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "Payload Control";
      ((System.ComponentModel.ISupportInitialize)(this.ud_numPayloads)).EndInit();
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btn_connect;
    private System.Windows.Forms.Button Arm;
    private System.Windows.Forms.TextBox tb_payloadStatus;
    private System.Windows.Forms.TextBox tb_connectionStatus;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox tb_lat;
    private System.Windows.Forms.NumericUpDown ud_numPayloads;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.TextBox tb_lng;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Button btn_updateTarget;
    private System.Windows.Forms.RadioButton rb_headwind;
    private System.Windows.Forms.RadioButton rb_tailwind;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TextBox tb_windspeed;
    private System.Windows.Forms.Button btn_manualDrop;
  }
}