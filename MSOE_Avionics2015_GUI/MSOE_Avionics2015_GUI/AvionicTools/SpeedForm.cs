﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSOE_Avionics2015_GUI.AvionicTools
{
  public partial class SpeedForm : Form
  {
    public SpeedForm()
    {
      InitializeComponent();
    }

    delegate void setSpeedCallback(float speed);

    public void setSpeed(float speed)
    {
      if (this.textBoxSpeed.InvokeRequired)
      {
        if(this.IsDisposed)
        {
          return;
        }
        setSpeedCallback d = new setSpeedCallback(setSpeed);
        try
        {
          this.Invoke(d, new object[] { speed });
        }
        catch { }
      }
      else
      {
        textBoxSpeed.Text = Convert.ToString(speed);
        this.Refresh();

      }
    }
    public void resetForm()
    {
      textBoxSpeed.Text = "0";
    }
  }
}
