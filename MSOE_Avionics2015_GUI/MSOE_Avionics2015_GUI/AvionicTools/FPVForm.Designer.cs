﻿namespace MSOE_Avionics2015_GUI.AvionicTools
{
  partial class FPVForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.sc_FPVPanel = new System.Windows.Forms.SplitContainer();
      this.RefreshFPVList = new System.Windows.Forms.Button();
      this.btn_openDevice = new System.Windows.Forms.Button();
      this.cmb_deviceList = new System.Windows.Forms.ComboBox();
      ((System.ComponentModel.ISupportInitialize)(this.sc_FPVPanel)).BeginInit();
      this.sc_FPVPanel.Panel2.SuspendLayout();
      this.sc_FPVPanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // sc_FPVPanel
      // 
      this.sc_FPVPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.sc_FPVPanel.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
      this.sc_FPVPanel.IsSplitterFixed = true;
      this.sc_FPVPanel.Location = new System.Drawing.Point(0, 0);
      this.sc_FPVPanel.Margin = new System.Windows.Forms.Padding(0);
      this.sc_FPVPanel.Name = "sc_FPVPanel";
      this.sc_FPVPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // sc_FPVPanel.Panel1
      // 
      this.sc_FPVPanel.Panel1.BackColor = System.Drawing.Color.Black;
      // 
      // sc_FPVPanel.Panel2
      // 
      this.sc_FPVPanel.Panel2.Controls.Add(this.RefreshFPVList);
      this.sc_FPVPanel.Panel2.Controls.Add(this.btn_openDevice);
      this.sc_FPVPanel.Panel2.Controls.Add(this.cmb_deviceList);
      this.sc_FPVPanel.Panel2MinSize = 60;
      this.sc_FPVPanel.Size = new System.Drawing.Size(611, 511);
      this.sc_FPVPanel.SplitterDistance = 447;
      this.sc_FPVPanel.TabIndex = 0;
      // 
      // RefreshFPVList
      // 
      this.RefreshFPVList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.RefreshFPVList.Location = new System.Drawing.Point(443, 16);
      this.RefreshFPVList.Name = "RefreshFPVList";
      this.RefreshFPVList.Size = new System.Drawing.Size(75, 29);
      this.RefreshFPVList.TabIndex = 2;
      this.RefreshFPVList.Text = "Refresh";
      this.RefreshFPVList.UseVisualStyleBackColor = true;
      this.RefreshFPVList.Click += new System.EventHandler(this.RefreshFPVList_Click);
      // 
      // btn_openDevice
      // 
      this.btn_openDevice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btn_openDevice.Location = new System.Drawing.Point(524, 16);
      this.btn_openDevice.Name = "btn_openDevice";
      this.btn_openDevice.Size = new System.Drawing.Size(75, 29);
      this.btn_openDevice.TabIndex = 1;
      this.btn_openDevice.Text = "Open";
      this.btn_openDevice.UseVisualStyleBackColor = true;
      this.btn_openDevice.Click += new System.EventHandler(this.btn_openDevice_Click);
      // 
      // cmb_deviceList
      // 
      this.cmb_deviceList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.cmb_deviceList.FormattingEnabled = true;
      this.cmb_deviceList.Location = new System.Drawing.Point(316, 19);
      this.cmb_deviceList.Name = "cmb_deviceList";
      this.cmb_deviceList.Size = new System.Drawing.Size(121, 24);
      this.cmb_deviceList.TabIndex = 0;
      // 
      // FPVForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(611, 511);
      this.Controls.Add(this.sc_FPVPanel);
      this.Location = new System.Drawing.Point(915, 0);
      this.MinimumSize = new System.Drawing.Size(323, 338);
      this.Name = "FPVForm";
      this.ShowIcon = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "FPV (First Person View)";
      this.sc_FPVPanel.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.sc_FPVPanel)).EndInit();
      this.sc_FPVPanel.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.SplitContainer sc_FPVPanel;
    private System.Windows.Forms.Button btn_openDevice;
    private System.Windows.Forms.ComboBox cmb_deviceList;
    private System.Windows.Forms.Button RefreshFPVList;
  }
}