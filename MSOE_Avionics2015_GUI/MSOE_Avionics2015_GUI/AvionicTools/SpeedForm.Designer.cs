﻿namespace MSOE_Avionics2015_GUI.AvionicTools
{
  partial class SpeedForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBoxSpeed = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.SuspendLayout();
      // 
      // textBoxSpeed
      // 
      this.textBoxSpeed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.textBoxSpeed.Enabled = false;
      this.textBoxSpeed.Location = new System.Drawing.Point(12, 26);
      this.textBoxSpeed.Name = "textBoxSpeed";
      this.textBoxSpeed.ReadOnly = true;
      this.textBoxSpeed.Size = new System.Drawing.Size(100, 22);
      this.textBoxSpeed.TabIndex = 0;
      this.textBoxSpeed.Text = "0";
      this.textBoxSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(37, 6);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(49, 17);
      this.label1.TabIndex = 1;
      this.label1.Text = "Speed";
      // 
      // label2
      // 
      this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(118, 28);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(88, 17);
      this.label2.TabIndex = 2;
      this.label2.Text = "Feet/Second";
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.BackgroundImage = global::MSOE_Avionics2015_GUI.Properties.Resources.Speed_Mock;
      this.splitContainer1.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.label2);
      this.splitContainer1.Panel2.Controls.Add(this.label1);
      this.splitContainer1.Panel2.Controls.Add(this.textBoxSpeed);
      this.splitContainer1.Panel2MinSize = 60;
      this.splitContainer1.Size = new System.Drawing.Size(217, 278);
      this.splitContainer1.SplitterDistance = 217;
      this.splitContainer1.SplitterWidth = 1;
      this.splitContainer1.TabIndex = 3;
      // 
      // SpeedForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
      this.ClientSize = new System.Drawing.Size(217, 278);
      this.Controls.Add(this.splitContainer1);
      this.DoubleBuffered = true;
      this.Location = new System.Drawing.Point(1535, 570);
      this.MinimumSize = new System.Drawing.Size(235, 323);
      this.Name = "SpeedForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "Speed";
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.Panel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

        #endregion

        private System.Windows.Forms.TextBox textBoxSpeed;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    private System.Windows.Forms.SplitContainer splitContainer1;
  }
}