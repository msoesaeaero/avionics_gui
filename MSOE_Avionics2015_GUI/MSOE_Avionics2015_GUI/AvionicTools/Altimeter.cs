﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSOE_Avionics2015_GUI.AvionicTools
{
  partial class AltimeterForm
  {
    private Altimeter _AltimeterControl;

    public Altimeter AltimeterControl
    {
      get
      {
        return _AltimeterControl;
      }

      set
      {
        _AltimeterControl = value;
      }
    }

    private void CalibrateButtonAlt_Click(object sender, EventArgs e)
    {
      _AltimeterControl.SetCalibrate();
    }
  }

  public class Altimeter : Avionic_Tool
  {
    private Form _Tool_Form;
    private Toolbox _Tool_Toolbox;
    private bool _Tool_isOpen;

    private bool isCalibrate;
    private float[] values = new float[5];
    private int valueCount = 0;

    public Form Tool_Form
    {
      get
      {
        return _Tool_Form;
      }

      set
      {
        _Tool_Form = value;
      }
    }

    public Toolbox Tool_Toolbox
    {
      get
      {
        return _Tool_Toolbox;
      }

      set
      {
        _Tool_Toolbox = value;
      }
    }

    public bool isOpen
    {
      get
      {
        return _Tool_isOpen;
      }
      set
      {
        _Tool_isOpen = value;
      }
    }

    public Int32 altimeterVal;

    public Altimeter()
    {
    }

    public void Tool_initTool(Toolbox tb)
    {
      altimeterVal = 0;
      isCalibrate = false;

      Tool_Toolbox = tb;
      Tool_Form = new AltimeterForm();
      ((AltimeterForm)Tool_Form).AltimeterControl = this;
      Tool_Form.FormClosed += new FormClosedEventHandler(Tool_Form_FormClosed);
      _Tool_isOpen = false;
    }

    public void Tool_open()
    {
      if (Tool_Form == null)
      {
        throw new MemberAccessException("Tool Form is null.");
      }
      if (!isOpen)
      {
        isOpen = true;
        Tool_Form.Show();
      }
    }

    public void Tool_close()
    {
      if (Tool_Form == null || Tool_Toolbox == null)
      {
        throw new MemberAccessException("A class member of the Altimeter tool is null.");
      }

      if (isOpen)
      {
        Tool_Form.Close();
      }
    }

    public void Tool_update(recStruct dataIn)
    {
      float average = 0;
      int i;
      if(valueCount < 5)
      {
        valueCount++;
      }

      for(i = 4; i > 0; i--) // Shift values to the end of array
      {
        values[i] = values[i-1];
      }
      values[0] = dataIn.altitude; // Add new value to start of array

      for(i = 0; i < valueCount; i++) // Sum up to the value Count
      {
        average += values[i];
      }

      average = average / valueCount; // Calculate average

      if (Tool_Toolbox.ShowAltimeter)
      {
        ((AltimeterForm)_Tool_Form).setAltitude(average);
        ((AltimeterForm)_Tool_Form).setDropped(dataIn.haveDropped, average);
      }
    }

    private void Tool_Form_FormClosed(object sender, FormClosedEventArgs e)
    {
      if (Tool_Form == null || Tool_Toolbox == null)
      {
        throw new MemberAccessException("A class member of the Altimeter tool is null.");
      }

      Tool_Toolbox.ShowAltimeter = false;
      isOpen = false;
    }

    public void SetCalibrate()
    {
      isCalibrate = true;
    }

    public bool CalibrateAltimeter()
    {
      bool ret = isCalibrate;
      isCalibrate = false;
      return ret;
    }

    public void reset()
    {
      AltimeterForm form = (AltimeterForm)_Tool_Form;
      valueCount = 0;
      form.resetBoxes();
    }
  }
}
