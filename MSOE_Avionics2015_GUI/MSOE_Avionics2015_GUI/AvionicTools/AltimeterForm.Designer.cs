﻿namespace MSOE_Avionics2015_GUI.AvionicTools
{
  partial class AltimeterForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tempLabel = new System.Windows.Forms.Label();
      this.CalibrateButtonAlt = new System.Windows.Forms.Button();
      this.textBoxAlt = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.textBoxDrop = new System.Windows.Forms.TextBox();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.SuspendLayout();
      // 
      // tempLabel
      // 
      this.tempLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.tempLabel.AutoSize = true;
      this.tempLabel.BackColor = System.Drawing.Color.Transparent;
      this.tempLabel.Location = new System.Drawing.Point(30, 19);
      this.tempLabel.Name = "tempLabel";
      this.tempLabel.Size = new System.Drawing.Size(63, 17);
      this.tempLabel.TabIndex = 0;
      this.tempLabel.Text = "Altimeter";
      // 
      // CalibrateButtonAlt
      // 
      this.CalibrateButtonAlt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.CalibrateButtonAlt.Location = new System.Drawing.Point(213, 9);
      this.CalibrateButtonAlt.Name = "CalibrateButtonAlt";
      this.CalibrateButtonAlt.Size = new System.Drawing.Size(88, 52);
      this.CalibrateButtonAlt.TabIndex = 1;
      this.CalibrateButtonAlt.Text = "Calibrate";
      this.CalibrateButtonAlt.UseVisualStyleBackColor = true;
      this.CalibrateButtonAlt.Click += new System.EventHandler(this.CalibrateButtonAlt_Click);
      // 
      // textBoxAlt
      // 
      this.textBoxAlt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.textBoxAlt.Enabled = false;
      this.textBoxAlt.Location = new System.Drawing.Point(14, 39);
      this.textBoxAlt.Name = "textBoxAlt";
      this.textBoxAlt.ReadOnly = true;
      this.textBoxAlt.Size = new System.Drawing.Size(95, 22);
      this.textBoxAlt.TabIndex = 2;
      this.textBoxAlt.Text = "----";
      this.textBoxAlt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.label1.AutoSize = true;
      this.label1.BackColor = System.Drawing.Color.Transparent;
      this.label1.Location = new System.Drawing.Point(119, 19);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(84, 17);
      this.label1.TabIndex = 3;
      this.label1.Text = "Drop Height";
      // 
      // textBoxDrop
      // 
      this.textBoxDrop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
      this.textBoxDrop.Enabled = false;
      this.textBoxDrop.Location = new System.Drawing.Point(115, 39);
      this.textBoxDrop.Name = "textBoxDrop";
      this.textBoxDrop.ReadOnly = true;
      this.textBoxDrop.Size = new System.Drawing.Size(92, 22);
      this.textBoxDrop.TabIndex = 4;
      this.textBoxDrop.Text = "----";
      this.textBoxDrop.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
      this.splitContainer1.IsSplitterFixed = true;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.BackgroundImage = global::MSOE_Avionics2015_GUI.Properties.Resources.Altimeter_Mock;
      this.splitContainer1.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.splitContainer1.Panel1MinSize = 10;
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.textBoxDrop);
      this.splitContainer1.Panel2.Controls.Add(this.CalibrateButtonAlt);
      this.splitContainer1.Panel2.Controls.Add(this.textBoxAlt);
      this.splitContainer1.Panel2.Controls.Add(this.label1);
      this.splitContainer1.Panel2.Controls.Add(this.tempLabel);
      this.splitContainer1.Panel2MinSize = 70;
      this.splitContainer1.Size = new System.Drawing.Size(312, 389);
      this.splitContainer1.SplitterDistance = 318;
      this.splitContainer1.SplitterWidth = 1;
      this.splitContainer1.TabIndex = 5;
      // 
      // AltimeterForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.ClientSize = new System.Drawing.Size(312, 389);
      this.Controls.Add(this.splitContainer1);
      this.DoubleBuffered = true;
      this.Location = new System.Drawing.Point(915, 570);
      this.MaximizeBox = false;
      this.MinimumSize = new System.Drawing.Size(330, 434);
      this.Name = "AltimeterForm";
      this.ShowIcon = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "Altimeter";
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.Panel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label tempLabel;
    private System.Windows.Forms.Button CalibrateButtonAlt;
        private System.Windows.Forms.TextBox textBoxAlt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxDrop;
    private System.Windows.Forms.SplitContainer splitContainer1;
  }
}