﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSOE_Avionics2015_GUI.AvionicTools
{
  partial class SpeedForm
  {
    private Speed _SpeedControl;

    public Speed SpeedControl
    {
      get
      {
        return _SpeedControl;
      }

      set
      {
        _SpeedControl = value;
      }
    }
  }

  public class Speed : Avionic_Tool
  {
    private Form _Tool_Form;
    private Toolbox _Tool_Toolbox;
    private bool _Tool_isOpen;


    public Form Tool_Form
    {
      get
      {
        return _Tool_Form;
      }

      set
      {
        _Tool_Form = value;
      }
    }

    public Toolbox Tool_Toolbox
    {
      get
      {
        return _Tool_Toolbox;
      }

      set
      {
        _Tool_Toolbox = value;
      }
    }
    public bool isOpen
    {
      get
      {
        return _Tool_isOpen;
      }
      set
      {
        _Tool_isOpen = value;
      }
    }
    public Int32 SpeedVal;

    public Speed()
    {
    }

    public void Tool_initTool(Toolbox tb)
    {
      SpeedVal = 0;

      Tool_Form = new SpeedForm();
      Tool_Form.FormClosed += new FormClosedEventHandler(Tool_Form_FormClosed);
      _Tool_isOpen = false;

    }

    public void Tool_update(recStruct dataIn)
    {
      if(Tool_Toolbox.ShowSpeed)
      {
        if(dataIn.gpsData.validity == 1)
        {
          ((SpeedForm)_Tool_Form).setSpeed(dataIn.gpsData.speed);
        }
      }
    }

    public void Tool_open()
    {
      if (Tool_Form == null)
      {
        throw new MemberAccessException("Tool Form is null.");
      }
      if (!isOpen)
      {
        isOpen = true;
        Tool_Form.Show();
      }
    }

    public void Tool_close()
    {
      if (Tool_Form == null || Tool_Toolbox == null)
      {
        throw new MemberAccessException("A class member of the Speed tool is null.");
      }

      if (isOpen)
      {
        Tool_Form.Close();
      }
    }

    private void Tool_Form_FormClosed(object sender, FormClosedEventArgs e)
    {
      if (Tool_Form == null || Tool_Toolbox == null)
      {
        throw new MemberAccessException("A class member of the Speed tool is null.");
      }

      Tool_Toolbox.ShowSpeed = false;
      isOpen = false;
    }

    public void reset()
    {
      SpeedForm form = (SpeedForm)_Tool_Form;
      form.resetForm();
      SpeedVal = 0;
    }
  }
}
