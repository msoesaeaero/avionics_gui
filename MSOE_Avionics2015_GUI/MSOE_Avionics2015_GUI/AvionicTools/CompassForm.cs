﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSOE_Avionics2015_GUI.AvionicTools
{
  public partial class CompassForm : Form
  {
    bool targetSet = false; //Flag to indicate if target has been updated
    double targetLat = 0;
    double targetLng = 0;
    double headingToTarget = 0;

    public CompassForm()
    {
      InitializeComponent();
    }

    delegate void setCompassCallback(gps_data gpsDataIn);

    public void setCompassLocation(gps_data gpsDataIn)
    {
      String longitude;
      String lat;

      if (this.textBoxLat.InvokeRequired || this.textBoxLong.InvokeRequired)
      {
        if(this.IsDisposed)
        {
          return;
        }
        setCompassCallback d = new setCompassCallback(setCompassLocation);
        try
        {
          this.Invoke(d, new object[] { gpsDataIn });
        }
        catch { }
      }
      else
      {
        if (gpsDataIn.latitude.indicator == 0) //If the indicator is EAST
        {
          lat = "N " + Convert.ToString(gpsDataIn.latitude.degrees) + "° " + Convert.ToString(gpsDataIn.latitude.minutes) + "'";
          textBoxLat.Text = lat;
        }
        else //If the indicator is WEST
        {
          lat = "S " + Convert.ToString(gpsDataIn.latitude.degrees) + "° " + Convert.ToString(gpsDataIn.latitude.minutes) + "'";
          textBoxLat.Text = lat;
        }

        if (gpsDataIn.longitude.indicator == 0) //If the indicator is NORTH
        {
          longitude = "E " + Convert.ToString(gpsDataIn.longitude.degrees) + "° " + Convert.ToString(gpsDataIn.longitude.minutes) + "'";
          textBoxLong.Text = longitude;
        }
        else //If the indicator is SOUTH
        {
          longitude = "W " + Convert.ToString(gpsDataIn.longitude.degrees) + "° " + Convert.ToString(gpsDataIn.longitude.minutes) + "'";
          textBoxLong.Text = longitude;
        }

        this.Refresh();

      }
    }

    public void resetForm()
    {
      textBoxLat.Text = "N/A";
      textBoxLong.Text = "N/A";
      textBoxHeading.Text = "N/A";
      textBoxToTarget.Text = "N/A";
      targetLat = 0;
      targetLng = 0;
      headingToTarget = 0;
      this.Refresh();
    }

    delegate void setCompassHeadingCallback(gps_data gpsDataIn);

    public void setCompassHeading(gps_data gpsDataIn) //Updates the heading text boxes
    {
      if (this.textBoxLat.InvokeRequired || this.textBoxLong.InvokeRequired)
      {
        if(this.IsDisposed)
        {
          return;
        }
        setCompassHeadingCallback d = new setCompassHeadingCallback(setCompassHeading);
        try
        {
          this.Invoke(d, new object[] { gpsDataIn });
        }
        catch { }
      }
      else
      {
        textBoxHeading.Text = Convert.ToString(gpsDataIn.heading) + "°";

        if (targetSet == true)
        {
          //Calculate bearing
          double dx = targetLat - (Convert.ToDouble(gpsDataIn.latitude.degrees) + (Convert.ToDouble(gpsDataIn.latitude.minutes)/60));
          double dy = targetLng + (Convert.ToDouble(gpsDataIn.longitude.degrees) + (Convert.ToDouble(gpsDataIn.longitude.minutes)/60));
          headingToTarget = Math.Atan2(dy, dx) * (180 / Math.PI);
          headingToTarget = Math.Round(headingToTarget, 4); //Round to four decimal places
          textBoxToTarget.Text = Convert.ToString(headingToTarget) + "°"; //Need to update angle needed to go over target
        }
        

        this.Refresh();
      }
    }

    public void updateTarget(double lat, double lng)
    {
      targetLat = lat;
      targetLng = lng;
      targetSet = true;
    }
  }
}

