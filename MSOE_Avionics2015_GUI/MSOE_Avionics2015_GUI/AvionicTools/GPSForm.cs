﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSOE_Avionics2015_GUI.AvionicTools
{
  public partial class GPSForm : Form
  {
    private GMap.NET.PointLatLng _targetLoc;
    private GMap.NET.WindowsForms.GMapMarker _targetMarker;
    private GMap.NET.WindowsForms.GMapRoute _planeRoute;
    private GMap.NET.WindowsForms.GMapOverlay _mapOverlay;
    private GMap.NET.WindowsForms.GMapOverlay _directionOverlay; //Used to draw the direction the plane is heading
    private GMap.NET.PointLatLng graftonPoint = new GMap.NET.PointLatLng(43.341503, -87.917436);
    private GMap.NET.PointLatLng texasPoint = new GMap.NET.PointLatLng(32.609621, -97.483479);
    private GMap.NET.PointLatLng athleticPoint = new GMap.NET.PointLatLng(43.044196, -87.906459);
    private Pen routePen;

    public GPSForm()
    {
      InitializeComponent();

      // Initial GMap settings
      GMap.NET.PointLatLng startPoint = texasPoint; //Initialize to texas
      gMap.Position = startPoint;
      gMap.MapProvider = GMap.NET.MapProviders.GMapProviders.BingSatelliteMap;
      gMap.MinZoom = 3;
      gMap.MaxZoom = 25;
      gMap.Zoom = 19;
      tb_zoom.Value = 19;
      gMap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
      gMap.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      gMap.Manager.Mode = GMap.NET.AccessMode.ServerAndCache; //Only want to access our current cache, can access internet if possible
      gMap.IgnoreMarkerOnMouseWheel = true;
      gMap.CanDragMap = true;

      // Initial map overlay settings
      _mapOverlay = new GMap.NET.WindowsForms.GMapOverlay("mainMapOverlay");

      //Initial direction overlay settings
      _directionOverlay = new GMap.NET.WindowsForms.GMapOverlay("directionOverlay");

      // Initial plane route settings
      _planeRoute = new GMap.NET.WindowsForms.GMapRoute("planeRoute");
      routePen = new Pen(Color.Red, 2);
      routePen.LineJoin = System.Drawing.Drawing2D.LineJoin.Round;
      _planeRoute.Stroke = routePen;

      // Put things in other things.
      _mapOverlay.Routes.Add(_planeRoute);
      gMap.Overlays.Add(_mapOverlay);
      gMap.Overlays.Add(_directionOverlay);

      // Set up airplane image/sprite
      Image planeSprite = Image.FromFile("../../assets/White_Plane_Logo.png");
      //Graphics formGraphics = this.CreateGraphics();
      //formGraphics.DrawImage(planeSprite, 125, 125);

      //Initialize the combo box selections
      comboGPS.Items.Add("SAE Aero East");
      comboGPS.Items.Add("Grafton");
      comboGPS.Items.Add("Athletic Field");

      // Test code to demonstrate a marker and a route
      //AddToRoute(new GMap.NET.PointLatLng(32.609621, -97.483479));
      //AddToRoute(new GMap.NET.PointLatLng(32.609721, -97.483579));
      //AddDropMarker(new GMap.NET.PointLatLng(32.609721, -97.483579));
      //AddToRoute(new GMap.NET.PointLatLng(32.609821, -97.483379));
    }

    delegate void SetMapPositionCallback(double lat, double lng);

    public void SetMapPosition(double lat, double lng)
    {
      if (this.gMap.InvokeRequired)
      {
        if(this.IsDisposed)
        {
          return;
        }
        Delegate d = new SetMapPositionCallback(SetMapPosition);
        try
        {
          this.Invoke(d, new object[] { lat, lng });
        }
        catch { }
      }
      else
      {
        GMap.NET.PointLatLng newPoint = new GMap.NET.PointLatLng(lat, lng);
        this.gMap.Position = newPoint;
        this.Refresh();
      }
    }

    delegate void SetTargetLocationCallback(double lat, double lng);

    public void SetTargetLocation(double lat, double lng)
    {
      if (this.gMap.InvokeRequired)
      {
        if(this.IsDisposed)
        {
          return;
        }
        Delegate d = new SetTargetLocationCallback(SetTargetLocation);
        try
        {
          this.Invoke(d, new object[] { lat, lng });
        }
        catch { }
      }
      else
      {
        // Remove current marker
        if (_targetMarker != null)
        {
          _mapOverlay.Markers.Remove(_targetMarker);
        }

        // Update target location
        _targetLoc.Lat = lat;
        _targetLoc.Lng = lng;

        

        // Place new marker at target location
        _targetMarker = new GMap.NET.WindowsForms.Markers.GMarkerGoogle(_targetLoc, GMap.NET.WindowsForms.Markers.GMarkerGoogleType.red_small);
        _mapOverlay.Markers.Add(_targetMarker);

        gMap.Update();
      }
    }

    delegate void AddToRouteCallback(GMap.NET.PointLatLng p);

    public void AddToRoute(GMap.NET.PointLatLng p)
    {
      if (this.gMap.InvokeRequired)
      {
        if(this.IsDisposed)
        {
          return;
        }
        Delegate d = new AddToRouteCallback(AddToRoute);
        try
        {
          this.Invoke(d, new object[] { p });
        }
        catch { }
      }
      else
      {
        _planeRoute.Points.Add(p);
        gMap.UpdateRouteLocalPosition(_planeRoute);
        gMap.Refresh();
      }
    }

    delegate void AddDropMarkerCallback(GMap.NET.PointLatLng p);

    public void AddDropMarker(GMap.NET.PointLatLng p)
    {
      if (this.gMap.InvokeRequired)
      {
        if(this.IsDisposed)
        {
          return;
        }
        Delegate d = new AddDropMarkerCallback(AddDropMarker);
        try
        {
          this.Invoke(d, new object[] { p });
        }
        catch { }
      }
      else
      {
        _mapOverlay.Markers.Add(new GMap.NET.WindowsForms.Markers.GMarkerGoogle(p, GMap.NET.WindowsForms.Markers.GMarkerGoogleType.blue_dot));
        gMap.Update();
      }
    }

    private void UpdateZoom(object sender, EventArgs e)
    {
      if(this.IsDisposed)
      {
        return;
      }
      gMap.Zoom = tb_zoom.Value;
    }

    private void GPSForm_closeMap(object sender, FormClosedEventArgs e)
    {
      // Attempt to free up resources (big hitter is the GMap queue)
      //gMap.Dispose();
    }

    private void GPSForm_mapStart(object sender, EventArgs e)
    {
      //gMap.ShowExportDialog();
      //if(gMap.ShowImportDialog())
      //{
      //string name = gMap.CacheLocation;
      //gMap.Manager.UseMemoryCache = true;
      //}
    }

    private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (comboGPS.Text == "Grafton") //Want to change the map to the Grafton airfield
      {
        this.gMap.Position = graftonPoint;
      }
      else if (comboGPS.Text == "SAE Aero East") //Want to change the map to the Texas airfield
      {
        this.gMap.Position = texasPoint;
      }
      else if (comboGPS.Text == "Athletic Field") //Want to change to the MSOE athletic field
      {
        this.gMap.Position = athleticPoint;
      }
    }

    delegate void updateDirectionCallback(double heading);

    public void updateDirection(double heading)
    {
      if (this.gMap.InvokeRequired)
      {
        if(this.IsDisposed)
        {
          return;
        }
        Delegate d = new updateDirectionCallback(updateDirection);
        try
        {
          this.Invoke(d, new object[] { heading });
        }
        catch { }
      }
      else
      {
        double x = 0.0005 * Math.Cos(heading*(Math.PI/180));
        double y = 0.0005 * Math.Sin(heading*(Math.PI/180));

        _directionOverlay.Clear(); //First clear the map
        GMap.NET.PointLatLng currPoint = _planeRoute.Points.Last<GMap.NET.PointLatLng>(); //Get the last point in the route
        GMap.NET.PointLatLng endPoint =  new GMap.NET.PointLatLng(currPoint.Lat + x, currPoint.Lng + y);

        List<GMap.NET.PointLatLng> list = new List<GMap.NET.PointLatLng>();
        list.Add(currPoint);
        list.Add(endPoint);

        GMap.NET.WindowsForms.GMapPolygon directionLine = new GMap.NET.WindowsForms.GMapPolygon(list, "Direction");
        directionLine.Stroke = new Pen(Color.GreenYellow, 2);
        _directionOverlay.Polygons.Add(directionLine);
      }
    }

    public void resetOverlay()
    {
      _mapOverlay.Clear();
      _planeRoute.Clear();
      _mapOverlay.Routes.Add(_planeRoute);
      _directionOverlay.Clear();
    }
  }
}
