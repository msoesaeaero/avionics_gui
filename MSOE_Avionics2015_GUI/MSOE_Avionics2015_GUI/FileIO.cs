﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MSOE_Avionics2015_GUI
{
  public class FileIO
  {
    public Toolbox _Toolbox;
    
    static private String datafFilename;
    static private FileStream fileWriteStream;

    public void newFile()
    {
      if (datafFilename != null)
      {
        fileWriteStream.Close();
      }
      String newFilename = "Flight Data ";
      DateTime curDate = System.DateTime.Now;
      newFilename += curDate.Month.ToString() + " ";
      newFilename += curDate.Day.ToString() + " ";
      newFilename += curDate.Hour.ToString() + " ";
      newFilename += curDate.Minute.ToString() + " ";
      fileWriteStream = new FileStream(newFilename, FileMode.OpenOrCreate, FileAccess.Write);
      datafFilename = newFilename;
    }

    public void fileWrite(recStruct data)
    {
      if (fileWriteStream.CanWrite)
      {
        int dataSize = System.Runtime.InteropServices.Marshal.SizeOf(typeof(recStruct));
        byte[] temp = new byte[dataSize];

        IntPtr ptr = System.Runtime.InteropServices.Marshal.AllocHGlobal(dataSize);
        System.Runtime.InteropServices.Marshal.StructureToPtr(data, ptr, true);
        System.Runtime.InteropServices.Marshal.Copy(ptr, temp, 0, dataSize);
        System.Runtime.InteropServices.Marshal.FreeHGlobal(ptr);
        fileWriteStream.Write(temp, 0, dataSize);
      }
    }

    //returns null if bad file is chosen or file cant be read
    public List<recStruct> fileReplay()
    {
      fileWriteStream.Close();

      string filepath;
      System.Windows.Forms.OpenFileDialog openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
      openFileDialog1.ShowDialog();
      
      filepath = openFileDialog1.FileName;
      openFileDialog1.Dispose();
      List<recStruct> dataGroup = new List<recStruct>();
      if (filepath != "")
      {
        FileStream fileReadStream = new FileStream(filepath, FileMode.Open, FileAccess.Read);

        if (fileReadStream.CanRead)
        {
          int readSize = System.Runtime.InteropServices.Marshal.SizeOf(typeof(recStruct));

          recStruct data = new recStruct();
          byte[] rawData = new byte[readSize];
          long packetsReceived = fileReadStream.Length / readSize;
          for (long i = 0; i < packetsReceived; i++)
          {
            fileReadStream.Read(rawData, 0, readSize);
            
            IntPtr ptr = System.Runtime.InteropServices.Marshal.AllocHGlobal(readSize);
            System.Runtime.InteropServices.Marshal.Copy(rawData, 0, ptr, readSize);
            data = (recStruct)System.Runtime.InteropServices.Marshal.PtrToStructure(ptr, typeof(recStruct));

            System.Runtime.InteropServices.Marshal.FreeHGlobal(ptr);

            dataGroup.Add(data);
          }
          fileReadStream.Close();
        }
      }
      return dataGroup;
    }
  }
}
